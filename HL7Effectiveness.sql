select
	p.Number as PaymentNumber
	,p.ServiceNumber
	,cr.PayorNumber
	,case
		when pp.PayorNumber is null then 'N'
		else 'Y'
	end as MatchingPatientPayor
	,pp.PolicyNumber
	,pp.GroupNumber
	,case
		when dem.AliasName is not null then 'Y'
		else 'N'
	end as MatchingHL7
	,cr.PatientNumber
	,p.Amount + isnull(a.Amount, 0) as NetPayment
	,case
		when pay.Name like '%self%' then 'Y'
		else 'N'
	end as SelfPay
	,case
		when s.TransToSvcNum <> '' then 'Y'
		else 'N'
	end as ServiceTransferred
	,s.TransToSvcNum
	,case
		when a2.number is not null then 'Y'
		else 'N'
	end as ServiceInvalidated
from payment p
left join adjustment a
	on p.number = a.PaymentNumber
		and a.TransType = 'P'
		and p.ServiceNumber = a.ServiceNumber
join CashReceipt cr
	on p.CashReceiptNumber = cr.Number
join service s
	on p.ServiceNumber = s.Number
left join adjustment a2
	on p.ServiceNumber = a2.Number
	and a2.AdjustmentTypeCode = '019'
left join PatientPayor pp
	on cr.PayorNumber = pp.PayorNumber
	and cr.PatientNumber = pp.PatientNumber
join patient pat
	on cr.PatientNumber = pat.Number
left join IHSI_DemographicImportTemplateUpdatable dem
	on pat.AliasName = dem.AliasName
	and (pp.PolicyNumber = dem.Ins1PolicyNumber
		or pp.PolicyNumber = dem.Ins2PolicyNumber)
	and (pp.GroupNumber = dem.Ins1GroupNumber
		or pp.GroupNumber = dem.Ins2GroupNumber
		or pp.GroupNumber = '')
join payor pay
	on cr.PayorNumber = pay.Number
group by
	p.Number
	,p.ServiceNumber
	,cr.PayorNumber
	,pp.PayorNumber
	,pp.PolicyNumber
	,pp.GroupNumber
	,pay.Name
	,dem.AliasName
	,cr.PatientNumber
	,p.Amount
	,a.Amount
	,s.TransToSvcNum
	,a2.Number


/*
*Note there is an error margin of about 1% on these numbers. I accepted this and moved forward with my analysis in thhe interest of time rather than tracking down the duplicates.

*There are a total of 71,823 records in the payor table for a total of $3,944,693
	*54,764 of these have matching HL7 data (or no group number) for a total of $3,177,714
	*17,060 do not have matching HL7 data for a total of $766,978
		*2,252 of these have comparable patient payor data for a total of $163,740

*/
