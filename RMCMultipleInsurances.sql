select
	x.AliasName
	,dem.Ins1Name
	,dem.Ins1PolicyNumber
	,dem.Ins1GroupNumber
	--,dem.ins2name
	--,dem.Ins2PolicyNumber
	--,dem.Ins2GroupNumber
	--,count(x.aliasname) as InsuranceCount
from (
	select
		dem.AliasName
		,dem.Ins1Name
		,dem.ins1policynumber
	from IHSI_DemographicImportTemplateUpdatable dem
	where dem.ins1name is not null
	group by
		dem.AliasName
		,dem.Ins1Name
		,dem.ins1policynumber
	)x
	join IHSI_DemographicImportTemplateUpdatable dem
		on x.AliasName = dem.AliasName
		and dem.ins1name is not null
group by
	x.AliasName
	,dem.Ins1Name
	,dem.Ins1PolicyNumber
	,dem.Ins1GroupNumber
	--,dem.ins2name
	--,dem.Ins2PolicyNumber
	--,dem.Ins2GroupNumber
having count(x.aliasname) >= 2
order by x.AliasName asc
--order by InsuranceCount desc

