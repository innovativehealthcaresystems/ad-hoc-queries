select
	pay.Name as PayorName
	,pay.CPIDNumericCode
	,pay.AddressLine1
	,pay.Number as PayorNumber
	--,count(s.Number) as CodedServices
	,isnull(s.PatientNumber,'N/A') as PatientNumber
	,s.amount * s.units as ServiceCharge
	,s.number as ServiceNumber
from payor pay
left join service s
	on pay.Number = s.PayorNumber
group by
	pay.Name
	,pay.AddressLine1
	,pay.Number
	,s.PatientNumber
	,s.Amount
	,s.Units
	,s.Number
	,pay.CPIDNumericCode
--order by CodedServices desc
order by pay.Number 