select 
	--count(distinct s.AliasName) PatientCountNoInsurance
	--,cast(datepart(year,s.ServiceDate) as char(4)) + cast(DATEPART(month,s.ServiceDate) as char(2)) as ServiceMonth
	--,sr.PatientNumber
	s.AliasName
	,sr.PatientNumber
	,sr.FromDate
	,MemberID
	,SentPayerName
	,RecdPayerName
	,CoverageType
	,GroupNumber
	,BenefitDescriptions
from IHSI_DemographicImportTemplateUpdatable s
join patient p
	on s.AliasName = p.AliasName
join service sr
	on p.Number = sr.PatientNumber
		and sr.Amount * sr.Units > 0.01
join IHSI_Eligibil_Import ei
	on sr.PatientNumber = right(ei.TraceNumber,9)
	and CoverageType not like '%inactive%'
where (Ins1Address1 = '1701 OAK PARK BLVD'
	or Ins1PolicyNumber in ('', '00000000000'))
	and sr.PayorNumber in ('0000001','1111111')
	and sr.TransToSvcNum = ''
	and sr.FromDate >= '2018-05-01'
group by s.AliasName
	,sr.PatientNumber
	,sr.FromDate
	,MemberID
	,SentPayerName
	,RecdPayerName
	,CoverageType
	,GroupNumber
	,BenefitDescriptions
--group by cast(datepart(year,s.ServiceDate) as char(4)) + cast(DATEPART(month,s.ServiceDate) as char(2))
--	,sr.PatientNumber
--order by ServiceMonth desc
order by sr.PatientNumber asc