-----------------Testing-----------------------
declare @startDate date = '20190101'
declare @endDate date = '20190331'
-----------------------------------------------

IF EXISTS (select * from SIVSystem.dbo.Company where DatabaseName = db_name() and User1 like 'E%')  --E = ED client type, otherwise run the Clinical code.

BEGIN  ---ED CLIENT CODE 

------------------ED Code------------------------
select *
into #tmpStep1
from (
	select count(distinct st.PatientNumber) as TotalEncounters
		,count(distinct s.PatientNumber) as BilledEncounters
		,isnull(s.ProviderNumber, st.ProviderNumber) as ProviderNumber
		,cast(isnull(s.FromDate, st.TI_DateOfService) as date) as DOS
	from IHSI_Staging st
	left join Service s
		on st.PatientNumber = s.PatientNumber
		and s.Amount * s.Units > 0.01
	where isnull(s.FromDate, st.TI_DateOfService) >= @startDate
		and isnull(s.FromDate, st.TI_DateOfService) < DATEADD(day, 1, @endDate)
	group by isnull(s.ProviderNumber, st.ProviderNumber)
		,cast(isnull(s.FromDate, st.TI_DateOfService) as date)
)A

--select ts1.*
select distinct p.OrganizationName as Location
	,ts1.ProviderNumber
	,ts1.DOS
	--,datename(dw, DATEPART(WEEKDAY, ts1.DOS)) as WeekDay
	,datename(dw, ts1.DOS) as WeekDayTest
	,DATEPART(WEEK, ts1.DOS) as WeekNumber
	,DATEPART(MONTH, ts1.DOS) as MOS
	,DATEPART(year, ts1.DOS) as YOS
	,ts1.TotalEncounters
	,ts1.BilledEncounters
	--,p.OrganizationName
from #tmpStep1 ts1
left join Provider p
	on ts1.ProviderNumber = p.Number

drop table #tmpStep1

END


---------------------CL Code--------------------------
ELSE


BEGIN

select *
into #tmpStep1C
from (
	select count(distinct csr.PatientNumber) as TotalEncounters
		,count(distinct s.PatientNumber) as BilledEncounters
		,ISNULL(s.ProviderNumber, csr.ProviderNumber) as ProviderNumber
		,cast(isnull(s.FromDate, cdr.TI_DateOfService) as date) as DOS
	from IHSI_ClinDetailRecord cdr
	left join IHSI_ClinSummaryRecord csr
		on cdr.Summary_ID = csr.Summary_ID
	left join service s
		on csr.PatientNumber = s.PatientNumber
		and cast(cdr.TI_DateOfService as date) = cast(s.FromDate as date)
		and s.Amount * s.Units > 0.01
	where isnull(s.FromDate, cdr.TI_DateOfService) >= @startDate
		and isnull(s.FromDate, cdr.TI_DateOfService) < DATEADD(day, 1, @endDate)
	group by cast(isnull(s.FromDate, cdr.TI_DateOfService) as date)
		,ISNULL(s.ProviderNumber, csr.ProviderNumber)
)A

select distinct p.OrganizationName as Location
	,ts1.ProviderNumber
	,ts1.DOS
	,datename(dw, ts1.DOS) as WeekDay
	,DATEPART(WEEK, ts1.DOS) as WeekNumber
	,DATEPART(MONTH, ts1.DOS) as MOS
	,DATEPART(year, ts1.DOS) as YOS
	,ts1.TotalEncounters
	,ts1.BilledEncounters
	--,p.OrganizationName
from #tmpStep1C ts1
left join Provider p
	on ts1.ProviderNumber = p.Number

drop table #tmpStep1C

end
-----------------------Final output-----------------

