select s.PatientNumber
	,s.Number as ServiceNumber
	,s.ServiceItemCode
	,s.amount * s.Units as charge
	,bbs.Balance
	,cast(s.FromDate as date) as DOS
	,s.ServiceStatusCode
	,s.UserSvcStatusCode
	,s.PayorNumber as ServicePayorNumber
	,cr.PayorNumber as CRPayorNumber
	--,par.Name as PayorName
	--,par.TypeCode
	--,pt.Description
	,pay.Amount as PaymentAmount
	,cast(pay.EnteredDate as date) as PaymentDate
	,pay.EOBReasonText
	,pn.Number as NoteNumber
	,pn.NoteField1 as PatientNotePart1
	,pn.NoteField2 as PatientNotePart2
	,pay.CashReceiptNumber
	,cr.BatchNumber
	,cr.ClaimNumber
	,c.PrintedDate as ClaimPrintDate
	--,cr.PayorNumber
from service s
join payment pay
	on s.Number = pay.ServiceNumber
	and pay.Amount = 0
join Payor par
	on s.PayorNumber = par.Number
join IHSI_PayorType pt
	on par.TypeCode = pt.Code
join IHSI_BalanceByService bbs
	on s.Number = bbs.ServiceNumber
join CashReceipt cr
	on pay.CashReceiptNumber = cr.Number
join claim c
	on cr.ClaimNumber = c.Number
left join PatientNote pn
	on s.PatientNumber = pn.PatientNumber
	and cast(pay.EnteredDate as date) = cast(pn.EnteredDate as date)
where s.ServiceStatusCode <> 40
	and s.PayorNumber in ('0000021','0002100')
	and cr.PayorNumber in ('0000021','0002100')

/*Amerigroup Request 11/8/2018

	*Open Accounts (s.servicestatuscode <> 40)
	*All 0 pays
	*Fields
		*PatientNumber
		*ServiceNumber
		*SICode
		*Charge
		*Balance
		*DOS
		*ServiceStatusCode
		*UserSvcStatusCode (should be 005 but per Audrey, in-house doesn't enter)
		*PayorNumber (2 Amerigroup Payors)
		*PayorName
		*PayorType
		*PaymentDate
		*PatientNotes (safeword: "medical record (the actual phrase)", "rec", "upload", "med", "welcome 2 ...", "erik")
		*PaymentAmount (will be 0)
		*EOB Reason Text (holds denial code)
		*Claim number
		*Claim date (last preferably)
		*Batch number and date that denial came on (this is on the Denial Report)
*/



/*Denise: 499, 788, 420 availity*/