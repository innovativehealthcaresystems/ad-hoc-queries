select s.PatientNumber
	,cast(s.fromdate as date) as DOS
	,s.entereddate
from service s
join service s2
	on s.PatientNumber = s2.PatientNumber
	--and cast(s.fromdate as date) = cast(s2.fromdate as date)
left join adjustment a
	on s2.number = a.servicenumber
	and a.adjustmenttypecode in ('045','080')
where s.comment like '%correc%'
	and s.entereddate >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0)
	and s.entereddate < DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	and a.ServiceNumber is not null