select pay.Number, pay.Name, CPIDNumericCode, COUNT(s.number) as ServicesUnderPayor
from Payor pay
join Service s
	on pay.Number = s.PayorNumber
where pay.Name like '%united%'
	or pay.Name like '%uhc%'
group by s.PayorNumber
	,pay.Number
	,pay.Name
	,pay.CPIDNumericCode
order by ServicesUnderPayor desc
