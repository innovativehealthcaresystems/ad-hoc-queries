select
	s.EnteredByID
	,st.Name
	,s.EnteredDate
	,s.PostedPeriod
	,s.Number
	,*
	--top 1 s.EnteredDate
from service s
join staff st
	on s.EnteredByID = st.Number
	and st.DepartmentCode = 'I10'
left join IHSI_ClinCodingEntries cce
	on s.Number = cce.ServiceNumber
where s.PostedPeriod < '201810'
and s.AdmissionNumber <> ''
and s.comment like '%corrected%'
and cce.ServiceNumber is null
and s.TransFromSvcNum = ''
order by s.postedperiod desc