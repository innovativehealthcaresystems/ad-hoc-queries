--USE [LA_OLOL_PEPA]
--GO
--/****** Object:  StoredProcedure [dbo].[IHSI_ReimbDenialQueue_New_REVISED]    Script Date: 11/16/2018 10:31:36 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO


--ALTER PROCEDURE [dbo].[IHSI_ReimbDenialQueue_New_REVISED]
--	@UserID char(4) = '',
--	@Type int = 1

--AS


/*
------ =============================================
--Author: Mark Underwood & Cheryl Weddle
--Create date:  2010-MAY-07
--Description:  Returns a list containing denied or aging insurance claims for reimbursement purposes.
		**USED FOR RETRIEVING THE AGING/DENIAL QUEUE IN ICIS.**

--Last Modified: 

	- 6/7/2016 at 2:43 PM by Cheryl per Jeff - Hitesh modified the logic to improve performance. 
		The one large query was broken out into three queries with temp tables.  The largest improvement was made for the first section which 
		joins to the MiddleGate table.  Previously OLOL took 30+ seconds to run and now it takes under 5 seconds.
		The original logic is at the bottom commented out.

	- 4/5/2016 at 11:11 AM by Cheryl per Jeff - I updated the WHERE clause of the top section for clients who use the IHSI_MG4_ActionList table 
		to allow for Aging items that were previously hidden from the main queue (Aging = ''), but are now show Aging = Y due to them being either 
		escalated or pended to appear in the Escalated/Pended queues.
			
	- 4/1/2016 at 3:03 PM by Cheryl per Jeff - Added logic to the new IF EXIST statement from 3/30/2016 to require new data in the IHSI_MG4_ActionList table 
		within the last 8 days, otherwise to run the logic without the MiddleGate process.

	- 3/30/2016 at 11:22 AM by Cheryl per Jeff - Added logic to allow this procedure to run for all databases and determine the
		appropriate logic to use for clients who utilize the IHSI_MG4_ActionList table and those who do not.
	
	- 3/28/2016 at 1:25 PM by Cheryl per Jeff - Applied the Zirmed-Rejection logic to all db's who use the MiddleGate table that was 
		previoulsy only applied to WDH. See notes below from 3/1/2016.

	- 3/1/2016 by Cheryl as a result of Amy's SR 17648 - After implementing the MiddleGate table, users were not able to search
		for "aging" claims that were rejected on Zirmed.  To resolve, I allowed all aging claims to be searchable, but hidden from 
		the main queue.  I added a new line to the WHERE clause to retrieve items where Aging = ''.
		***NOTE:  Originally this was only added to WDH.  It was later applied to all db's who utilize the MiddleGate table on 3/28/2016 at 1:25 PM.

	- 2/9/2016 by Cheryl per Jeff - Added logic to utilize the new IHSI_MG4_ActionList table for "Aging" accounts only.  
		Claim Numbers in this table with a Priority of Medium or High (not Low) will be visible in the queue for Aging.
			
		Applied to these clients initially, but it's only being updated regularly for CES and AMH so far (as of 4/1/2016):
			IL_GCH_GES
			IL_MMCI_CES
			LA_OLOL_PEPA
			NH_WDH_SEPP
			TX_AMH_AEMA
			TX_JPS_IES
	
	- 10/21/2014 by Cheryl (AGS Project 20) - Updated to allow for services with a status code 99 to appear in the results.

	- 7/17/2012 by CW per Christie on a follow up to SR 7788 - Removed the prevoiusly added filtering of items with secondary payors that was added on 7/10/2012.
		Removed the filtering b/c the crossover payors' denials come through Zirmed and aren't posted in Cortex, rather Reimb works the accounts directly from Zirmed.
		This will allow the items to appear in the Reimb Queue as an Aging account to be worked, but they will appear with the primary payor listed as the claim payor, 
		since the claim was sent as a crossover claim from the primary vs. being sent through Cortex and the pmt comment field may show the denial message from the primary payor.
	
	- 7/10/2012 by CW per Christie on SR 7788 - Filter items with secondary payors, unless the payment payor and the claim payor are the same OR if the aging is > 30 days.
		
	- 4/26/2012 by Mark per Christie on SR 7439 - Removed section of code that retrieved the last activity when it was only "viewed" with no actions.
	
	- 4/19/2012 by CW per Christie - Don't show the Age of Denial, Denial Date, Deposit #, Batch #, Payment Payor and Payment Payor Phone information
		when the account has a previous denial older than the either the service's entered date or claim's printed date.
	
	- 6/30/2011 by CW per Desiree on SR 5432: Cleaned up the code and moved a section of code from the WHERE clause to a JOIN.
	
------ =============================================
*/

	---- SET NOCOUNT ON to prevent extra result sets FROM interfering w/ SELECT statements.
	SET NOCOUNT ON;


------------TESTING STORED PROCEDURE -----------
DECLARE
@UserID char(4)
----, @Type int

SET @UserID = 'T018'
----SET @Type = 2

----------------------------------------------

--Run this code for clients using the MiddleGate process, else run the 2nd set of code.  Requires new data in the MiddleGate table within last 8 days.
--IF EXISTS (
--	select MAX(DATEADD(MILLISECOND, reportDate % 1000, DATEADD(SECOND, reportdate / 1000, '19700101')))
--	from dbo.IHSI_MG4_ActionList
--	having MAX(DATEADD(MILLISECOND, reportDate % 1000, DATEADD(SECOND, reportdate / 1000, '19700101'))) > dateadd(dd, -8, getdate())
--	)


BEGIN 

-----Retrieve eligible claims from the IHSI_MG4_ActionList table.
	select mg.claimID  
	into #tmpClaim
	from dbo.IHSI_MG4_ActionList mg
		LEFT JOIN dbo.IHSI_MG4_ActionList as mg2 with(nolock) on mg.claimId = mg2.claimId  --2/9/2016 - NEW: pulls the max ReportDate
			AND mg.ReportDate < mg2.ReportDate
	where mg.[priority] <> 'low' and mg2.[priority] is null 

	select * from #tmpClaim
----Limit the records further.
	select rq.PatientNumber
		, rq.ClaimNumber
		, rq.AssignedToID
		, rq.Denial
		, rq.Aging
		, (CASE
				WHEN rq.Aging = 'Y' AND rq.Denial = 'Y' THEN 'AD'
				WHEN rq.Aging = 'Y' AND rq.Denial = 'N' THEN 'A'
				WHEN rq.Aging = 'N' AND rq.Denial = 'Y' THEN 'D'
				ELSE 'U'
		   END) as Type
		, rq.Pending
		, rq.PendingCoding
		, ra.ActivityEndTime as ActivityDate
		, rq.Escalated
		, rq.AssignedToID as AssignedTo
		, ra.Comments as EscalateNote
		, ra.WorkedByID as WorkedBy
		, ra.ActivityEndTime as LastWorkedDate
		, ra.ActivityID
		, rq.Active
	into #tmpReimbAct1
	from dbo.IHSI_ReimbQueue as rq with(nolock) 
		LEFT JOIN dbo.IHSI_ReimbActivity as RA with(nolock) on RA.queueID = RQ.queueID 
			AND (ra.ActivityID IN  
				(SELECT MAX(RA.ActivityID) as ActivityID 
				FROM dbo.IHSI_ReimbQueue as RQ with(nolock)
					INNER JOIN dbo.IHSI_ReimbActivity as RA with(nolock) on RA.queueID = RQ.queueID
					LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA with(nolock) on RARA.ActivityID = RA.ActivityID
				WHERE rara.ActivityID IS NOT NULL 
				GROUP BY RQ.QueueID) 
			OR ra.ActivityID IS NULL)
	where
		(
			(RQ.Active = 'Y' and rq.Denial = 'Y' ) --2/9/2016 - Combined to not allow just Active = Y items to be visible.

			--2/9/2016 - This OR statement is completely new for the MGActionList table. Includes Aging claims if the max ReportDate has a priority <> Low.
			OR (rq.Active = 'Y' 
				and rq.Aging = 'Y' 
				and (
					rq.ClaimNumber in (
							select claimID  from #tmpClaim
							)
					OR (rq.Escalated = 'Y' OR rq.Pending = 'Y' OR rq.PendingCoding = 'Y') --4/5/2016: Added to allow the previously hidden Aging items (Aging = '') that have been Escalated/Pended and now show Aging = 'Y' to be viewable in the Escalated/Pending queues.
					)
				) 
			OR (rq.Active = 'Y' and rq.Aging = '' ) --3/1/2016 - added to allow for all Aging claims to be searched, but not necessarily appear in the main queue.
			OR (ra.ActivityEndTime >= Convert(Datetime, Convert(char(10), GetDate(), 101)) AND ra.WorkedByID = @UserID) --unchanged from logic prior to 2/9/2016
		)
		AND rq.QueueLock = 'N'
		AND (rq.AssignedToID = @UserID OR rq.AssignedToID IS NULL OR rq.AssignedToID = '') -- Comment out this line if you want to run as a query

		--select * from #tmpReimbAct1

----Lastly, summarize data from above and do more filtering.
	SELECT s.ProviderNumber
		, p.number as PtNum 
		, pay.name as ClmPayorName  
		, pay.typecode as ClmPayorType 
		, CASE 
			WHEN isnull(pay2.name, '') = '' --shows Aging payor's "Age of Pmt/Denial" IF available
				or s.EnteredDate > pmt.EnteredDate
				or clm.PrintedDate > pmt.EnteredDate
			THEN '' ELSE pay2.name 
			END AS PmtPayorName
		, CASE 
			WHEN isnull(pay2.typecode, '') = '' --shows Aging payor's "Age of Pmt/Denial" IF available
				or s.EnteredDate > pmt.EnteredDate
				or clm.PrintedDate > pmt.EnteredDate
			THEN '' ELSE pay2.typecode
			END AS PmtPayorType
		, clm.number as ClaimNum
		, NULL as Balance
		, (CASE WHEN isnull(cr.depositnumber, '') = '' 	--shows only the Aging payor's EOB Reason, if available
				or s.EnteredDate > pmt.EnteredDate
				or clm.PrintedDate > pmt.EnteredDate 
			THEN '' ELSE pmt.eobReasontext END) as PmtComment
		
		, (CASE WHEN isnull(cr.depositnumber, '') = ''  --shows only the Aging payor's Pmt Date, if available
				or s.EnteredDate > pmt.EnteredDate
				or clm.PrintedDate > pmt.EnteredDate 
			THEN '' ELSE pmt.EnteredDate END) as PaymentDate  
		, clm.PrintedDate as ClaimDate
		, s.ServiceStatusCode
		, (CASE  
			WHEN DATEDIFF(day, clm.printeddate, Getdate()) <= '30' THEN 'Current'
			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '31' AND '60'  THEN 'Amount>30'
			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '61' AND '90'  THEN 'Amount>60'
			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '91' AND '120'  THEN 'Amount>90'
			WHEN DATEDIFF(day, clm.printeddate, Getdate()) > '120' THEN 'Amount>120'
			ELSE  'Unknown'
		  END) as AgingBucket
		, DATEDIFF(day, clm.printeddate, Getdate()) as AgingDays
		, ISNULL(s.FromDate, '1900-01-01') as DateOfService
		, ISNULL(cl.Name, '') as ClinicianName
		, tmp.Denial
		, tmp.Aging
		, (CASE
				WHEN tmp.Aging = 'Y' AND tmp.Denial = 'Y' THEN 'AD'
				WHEN tmp.Aging = 'Y' AND tmp.Denial = 'N' THEN 'A'
				WHEN tmp.Aging = 'N' AND tmp.Denial = 'Y' THEN 'D'
				ELSE 'U'
		   END) as Type
		, tmp.Pending
		, tmp.PendingCoding
		, tmp.ActivityDate
		, tmp.Escalated
		, tmp.AssignedTo
		, tmp.EscalateNote
		, tmp.WorkedBy
		, rr.Description as PendingReason
		, pp.PolicyNumber
		, tmp.LastWorkedDate
	FROM #tmpReimbAct1 tmp
	-- dbo.IHSI_ReimbQueue as rq with(nolock) 
		LEFT JOIN dbo.patient as p with(nolock) ON p.Number = tmp.Patientnumber
		LEFT JOIN dbo.claim as clm with(nolock) ON tmp.claimnumber = clm.number
		LEFT JOIN dbo.Service as s with(nolock) ON s.ClaimNumber = clm.number
		LEFT JOIN dbo.service as s2 with(nolock) ON s.ClaimNumber = s2.ClaimNumber 
			AND s.number < s2.number AND s2.ServiceStatusCode IN ('3', '4', '23', '24', '99')
		LEFT JOIN dbo.payment as pmt with(nolock) ON pmt.ServiceNumber = s.Number 
		LEFT JOIN dbo.payment as pmt2 with(nolock) ON pmt.servicenumber = pmt2.servicenumber 
			AND pmt.number < pmt2.number
		LEFT JOIN dbo.cashreceipt as cr with(nolock) ON pmt.cashreceiptnumber = cr.number
		--LEFT JOIN dbo.payor as pay with(nolock) ON isnull(cr.payornumber, clm.payornumber) = pay.number  --the isnull replaces the case stmts in the SELECT list
		LEFT JOIN dbo.payor as pay with(nolock) ON clm.payornumber = pay.number
		LEFT JOIN dbo.payor as pay2 with(nolock) ON cr.payornumber = pay2.number
		LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment rar with(nolock) on tmp.ActivityID = rar.ActivityID 
			AND (rar.ReasonID IN (SELECT ReasonID FROM dbo.IHSI_ReimbReason WHERE type = 'P'))
		LEFT JOIN dbo.IHSI_ReimbReason rr on rar.ReasonID = rr.ReasonID AND rr.Type = 'P'
		LEFT JOIN dbo.Clinician as cl with(nolock) ON s.PerformedByID = cl.Number
		LEFT JOIN dbo.PatientPayor pp on pp.PayorNumber = clm.PayorNumber AND pp.PatientNumber = clm.PatientNumber			

		left join #tmpClaim tc
			on tmp.ClaimNumber = tc.claimId

	WHERE s.servicestatuscode IN ('3', '4', '23', '24', '99')
		AND pmt2.number IS NULL
		AND s2.number IS NULL 
		and tc.claimId is not null
	ORDER BY ClaimDate, ClaimNum

DROP TABLE #tmpReimbAct1
DROP TABLE #tmpClaim

END


--ELSE --Run this code for clients who don't use the MiddleGate process.


--BEGIN


------First, limit the records.
--	select rq.PatientNumber
--		, rq.ClaimNumber
--		, rq.AssignedToID
--		, rq.Denial
--		, rq.Aging
--		, (CASE
--				WHEN rq.Aging = 'Y' AND rq.Denial = 'Y' THEN 'AD'
--				WHEN rq.Aging = 'Y' AND rq.Denial = 'N' THEN 'A'
--				WHEN rq.Aging = 'N' AND rq.Denial = 'Y' THEN 'D'
--				ELSE 'U'
--		   END) as Type
--		, rq.Pending
--		, rq.PendingCoding
--		, ra.ActivityEndTime as ActivityDate
--		, rq.Escalated
--		, rq.AssignedToID as AssignedTo
--		, ra.Comments as EscalateNote
--		, ra.WorkedByID as WorkedBy
--		, ra.ActivityEndTime as LastWorkedDate
--		, ra.ActivityID
--	into #tmpReimbAct
--	from IHSI_ReimbQueue RQ
--		left join IHSI_ReimbActivity RA on RA.QueueID=RQ.QueueID 
--			AND (ra.ActivityID IN  --Added 2010/10/25 --moved back to a join on 6/30/11 by CW: Retrieves the last activity.
--				(SELECT MAX(RA1.ActivityID) as ActivityID 
--					FROM dbo.IHSI_ReimbQueue as RQ with(nolock)
--						  INNER JOIN dbo.IHSI_ReimbActivity as RA1 with(nolock) on RA1.queueID = RQ.queueID
--						  LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA with(nolock) on RARA.ActivityID = RA1.ActivityID
--					WHERE rara.ActivityID IS NOT NULL 				
--					--OR (rara.ActivityID IS NULL  ---2012-04-26: Removed to allow the last Worked Date to pick up items with an Action vs. ones that are only viewed.
--					--	AND RA1.queueID NOT IN (
--					--		SELECT RA2.queueID 
--					--		FROM dbo.IHSI_ReimbActivity as RA2 with(nolock)
--					--			INNER JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA2 with(nolock) on RARA2.ActivityID = RA2.ActivityID
--					--		GROUP BY RA2.queueID))
--						GROUP BY RQ.QueueID
--					) 
--			OR ra.ActivityID IS NULL)
--		WHERE (
--				rq.Active = 'Y' 
--				OR rq.Denial = 'Y' 
--				OR rq.Aging = 'Y'
--				OR (ra.ActivityEndTime >= Convert(Datetime, Convert(char(10), GetDate(), 101)) AND ra.WorkedByID = @UserID)
--				)		
--			AND rq.QueueLock = 'N'
--			AND (rq.AssignedToID = @UserID OR rq.AssignedToID IS NULL OR rq.AssignedToID = '')



------Lastly, summarize data from above and do more filtering.
--	SELECT s.ProviderNumber
--		, p.number as PtNum 
--		, pay.name as ClmPayorName  
--		, pay.typecode as ClmPayorType 
--		, CASE 
--			WHEN isnull(pay2.name, '') = '' --shows Aging payor's "Age of Pmt/Denial" IF available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate
--			THEN '' ELSE pay2.name 
--			END AS PmtPayorName
--		, CASE 
--			WHEN isnull(pay2.typecode, '') = '' --shows Aging payor's "Age of Pmt/Denial" IF available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate
--			THEN '' ELSE pay2.typecode
--			END AS PmtPayorType
--		, clm.number as ClaimNum
--		, NULL as Balance
--		, (CASE WHEN isnull(cr.depositnumber, '') = '' 	--shows only the Aging payor's EOB Reason, if available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate 
--			THEN '' ELSE pmt.eobReasontext END) as PmtComment
--		, (CASE WHEN isnull(cr.depositnumber, '') = ''  --shows only the Aging payor's Pmt Date, if available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate 
--			THEN '' ELSE pmt.EnteredDate END) as PaymentDate  
--		, clm.PrintedDate as ClaimDate
--		, s.ServiceStatusCode
--		, (CASE  
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) <= '30' THEN 'Current'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '31' AND '60'  THEN 'Amount>30'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '61' AND '90'  THEN 'Amount>60'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '91' AND '120'  THEN 'Amount>90'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) > '120' THEN 'Amount>120'
--			ELSE  'Unknown'
--		  END) as AgingBucket
--		, DATEDIFF(day, clm.printeddate, Getdate()) as AgingDays
--		, ISNULL(s.FromDate, '1900-01-01') as DateOfService
--		, ISNULL(cl.Name, '') as ClinicianName
--		, TMP.Denial
--		, TMP.Aging
--		, (CASE
--				WHEN TMP.Aging = 'Y' AND TMP.Denial = 'Y' THEN 'AD'
--				WHEN TMP.Aging = 'Y' AND TMP.Denial = 'N' THEN 'A'
--				WHEN TMP.Aging = 'N' AND TMP.Denial = 'Y' THEN 'D'
--				ELSE 'U'
--		   END) as Type
--		, TMP.Pending
--		, TMP.PendingCoding
--		, TMP.ActivityDate
--		, TMP.Escalated
--		, TMP.AssignedTo
--		, TMP.EscalateNote
--		, TMP.WorkedBy
--		, rr.Description as PendingReason
--		, pp.PolicyNumber
--		, TMP.LastWorkedDate
--	FROM #tmpReimbAct TMP
--	--dbo.IHSI_ReimbQueue as rq with(nolock) 
--		LEFT JOIN dbo.patient as p with(nolock) ON p.Number = TMP.Patientnumber
--		LEFT JOIN dbo.claim as clm with(nolock) ON TMP.claimnumber = clm.number
--		LEFT JOIN dbo.Service as s with(nolock) ON s.ClaimNumber = clm.number
--		LEFT JOIN dbo.service as s2 with(nolock) ON s.ClaimNumber = s2.ClaimNumber 
--			AND s.number < s2.number AND s2.ServiceStatusCode IN ('3', '4', '23', '24', '99')
--		LEFT JOIN dbo.payment as pmt with(nolock) ON pmt.ServiceNumber = s.Number 
--		LEFT JOIN dbo.payment as pmt2 with(nolock) ON pmt.servicenumber = pmt2.servicenumber 
--			AND pmt.number < pmt2.number
--		LEFT JOIN dbo.cashreceipt as cr with(nolock) ON pmt.cashreceiptnumber = cr.number
--		--LEFT JOIN dbo.payor as pay with(nolock) ON isnull(cr.payornumber, clm.payornumber) = pay.number  --the isnull replaces the case stmts in the SELECT list
--		LEFT JOIN dbo.payor as pay with(nolock) ON clm.payornumber = pay.number
--		LEFT JOIN dbo.payor as pay2 with(nolock) ON cr.payornumber = pay2.number		
--		LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment rar with(nolock) on TMP.ActivityID = rar.ActivityID 
--			AND (rar.ReasonID IN (SELECT ReasonID FROM dbo.IHSI_ReimbReason WHERE type = 'P'))
--		LEFT JOIN dbo.IHSI_ReimbReason rr on rar.ReasonID = rr.ReasonID AND rr.Type = 'P'
--		LEFT JOIN dbo.Clinician as cl with(nolock) ON s.PerformedByID = cl.Number
--		LEFT JOIN dbo.PatientPayor pp on pp.PayorNumber = clm.PayorNumber AND pp.PatientNumber = clm.PatientNumber
--	WHERE s.servicestatuscode IN ('3', '4', '23', '24', '99')
--		AND pmt2.number IS NULL
--		AND s2.number IS NULL 
--		--AND 1 = CASE --added 7/10/2012, then removed on 7/17/2012: eliminates records for secondary payors, except when the pmt payor and the clm payors are the same or if the aging is > 30 days.
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number = Pay2.Number THEN 1  
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number <> Pay2.Number and ABS(DATEDIFF(day, clm.PrintedDate, Getdate())) > 30 THEN 1 
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number <> Pay2.Number and ABS(DATEDIFF(day, clm.PrintedDate, Getdate())) < 30 THEN 2
--		--			ELSE 1
--		--		END
--	ORDER BY ClaimDate, ClaimNum

--DROP TABLE #tmpReimbAct

--END


--/*

-------Logic prior to changes on 6/7/2016



----Run this code for clients using the MiddleGate process, else run the 2nd set of code.  Requires new data in the MiddleGate table within last 8 days.
--IF EXISTS (
--	select MAX(DATEADD(MILLISECOND, reportDate % 1000, DATEADD(SECOND, reportdate / 1000, '19700101')))
--	from dbo.IHSI_MG4_ActionList
--	having MAX(DATEADD(MILLISECOND, reportDate % 1000, DATEADD(SECOND, reportdate / 1000, '19700101'))) > dateadd(dd, -8, getdate())
--	)

--BEGIN 

--	SELECT s.ProviderNumber
--		, p.number as PtNum 
--		, pay.name as ClmPayorName  
--		, pay.typecode as ClmPayorType 
--		, CASE 
--			WHEN isnull(pay2.name, '') = '' --shows Aging payor's "Age of Pmt/Denial" IF available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate
--			THEN '' ELSE pay2.name 
--			END AS PmtPayorName
--		, CASE 
--			WHEN isnull(pay2.typecode, '') = '' --shows Aging payor's "Age of Pmt/Denial" IF available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate
--			THEN '' ELSE pay2.typecode
--			END AS PmtPayorType
--		, clm.number as ClaimNum
--		, NULL as Balance
--		, (CASE WHEN isnull(cr.depositnumber, '') = '' 	--shows only the Aging payor's EOB Reason, if available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate 
--			THEN '' ELSE pmt.eobReasontext END) as PmtComment
		
--		, (CASE WHEN isnull(cr.depositnumber, '') = ''  --shows only the Aging payor's Pmt Date, if available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate 
--			THEN '' ELSE pmt.EnteredDate END) as PaymentDate  
--		, clm.PrintedDate as ClaimDate
--		, s.ServiceStatusCode
--		, (CASE  
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) <= '30' THEN 'Current'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '31' AND '60'  THEN 'Amount>30'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '61' AND '90'  THEN 'Amount>60'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '91' AND '120'  THEN 'Amount>90'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) > '120' THEN 'Amount>120'
--			ELSE  'Unknown'
--		  END) as AgingBucket
--		, DATEDIFF(day, clm.printeddate, Getdate()) as AgingDays
--		, ISNULL(s.FromDate, '1900-01-01') as DateOfService
--		, ISNULL(cl.Name, '') as ClinicianName
--		, rq.Denial
--		, rq.Aging
--		, (CASE
--				WHEN rq.Aging = 'Y' AND rq.Denial = 'Y' THEN 'AD'
--				WHEN rq.Aging = 'Y' AND rq.Denial = 'N' THEN 'A'
--				WHEN rq.Aging = 'N' AND rq.Denial = 'Y' THEN 'D'
--				ELSE 'U'
--		   END) as Type
--		, rq.Pending
--		, rq.PendingCoding
--		, ra.ActivityEndTime as ActivityDate
--		, rq.Escalated
--		, rq.AssignedToID as AssignedTo
--		, ra.Comments as EscalateNote
--		, ra.WorkedByID as WorkedBy
--		, rr.Description as PendingReason
--		, pp.PolicyNumber
--		, ra.ActivityEndTime as LastWorkedDate
--	FROM dbo.IHSI_ReimbQueue as rq with(nolock) 
--		LEFT JOIN dbo.patient as p with(nolock) ON p.Number = rq.Patientnumber
--		LEFT JOIN dbo.claim as clm with(nolock) ON rq.claimnumber = clm.number
--		LEFT JOIN dbo.Service as s with(nolock) ON s.ClaimNumber = clm.number
--		LEFT JOIN dbo.service as s2 with(nolock) ON s.ClaimNumber = s2.ClaimNumber 
--			AND s.number < s2.number AND s2.ServiceStatusCode IN ('3', '4', '23', '24', '99')
--		LEFT JOIN dbo.payment as pmt with(nolock) ON pmt.ServiceNumber = s.Number 
--		LEFT JOIN dbo.payment as pmt2 with(nolock) ON pmt.servicenumber = pmt2.servicenumber 
--			AND pmt.number < pmt2.number
--		LEFT JOIN dbo.cashreceipt as cr with(nolock) ON pmt.cashreceiptnumber = cr.number
--		--LEFT JOIN dbo.payor as pay with(nolock) ON isnull(cr.payornumber, clm.payornumber) = pay.number  --the isnull replaces the case stmts in the SELECT list
--		LEFT JOIN dbo.payor as pay with(nolock) ON clm.payornumber = pay.number
--		LEFT JOIN dbo.payor as pay2 with(nolock) ON cr.payornumber = pay2.number
--		LEFT JOIN dbo.IHSI_ReimbActivity as RA with(nolock) on RA.queueID = RQ.queueID 
--			AND (ra.ActivityID IN  --Added 2010/10/25 --moved back to a join on 6/30/11 by CW: Retrieves the last activity.
--				(SELECT MAX(RA.ActivityID) as ActivityID 
--				FROM dbo.IHSI_ReimbQueue as RQ with(nolock)
--					  INNER JOIN dbo.IHSI_ReimbActivity as RA with(nolock) on RA.queueID = RQ.queueID
--					  LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA with(nolock) on RARA.ActivityID = RA.ActivityID
--				WHERE rara.ActivityID IS NOT NULL 
				
--				--OR (rara.ActivityID IS NULL  ---2012-04-26: Removed to allow the last Worked Date to pick up items with an Action vs. ones that are only viewed.
--				--	AND ra.queueID NOT IN (
--				--		SELECT RA2.queueID 
--				--		FROM dbo.IHSI_ReimbActivity as RA2 with(nolock)
--				--			INNER JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA2 with(nolock) on RARA2.ActivityID = RA2.ActivityID
--				--		GROUP BY RA2.queueID))
				
--					GROUP BY RQ.QueueID) 
--				OR ra.ActivityID IS NULL)
										
--			--OLD VERSION:  This part of the join was moved to the where clause to improve performance (MAU 2010/10/25)
--				--AND (ra.ActivityID IN
--				--(SELECT MAX(RA.ActivityID) as ActivityID --RA.WorkedByID, 
--				--	FROM dbo.IHSI_ReimbQueue as RQ with(nolock)
--				--		  INNER JOIN dbo.IHSI_ReimbActivity as RA with(nolock) on RA.queueID = RQ.queueID
--			--		  INNER JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA with(nolock) on RARA.ActivityID = RA.ActivityID
--			--		GROUP BY RQ.QueueID) OR ra.ActivityID IS NULL)											
--		LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment rar with(nolock) on RA.ActivityID = rar.ActivityID 
--			AND (rar.ReasonID IN (SELECT ReasonID FROM dbo.IHSI_ReimbReason WHERE type = 'P'))
--		LEFT JOIN dbo.IHSI_ReimbReason rr on rar.ReasonID = rr.ReasonID AND rr.Type = 'P'
--		LEFT JOIN dbo.Clinician as cl with(nolock) ON s.PerformedByID = cl.Number
--		LEFT JOIN dbo.PatientPayor pp on pp.PayorNumber = clm.PayorNumber AND pp.PatientNumber = clm.PatientNumber			
--	WHERE 
--		----Original logic prior to changes on 2/9/2016 by Cheryl
--		--(
--		--	rq.Active = 'Y' 
--		--	OR rq.Denial = 'Y' 
--		--	OR rq.Aging = 'Y'   
--		--	OR (ra.ActivityEndTime >= Convert(Datetime, Convert(char(10), GetDate(), 101)) AND ra.WorkedByID = @UserID)
--		--	)
			
--		----New logic on 2/9/2016 by Cheryl
--		(
--			(rq.Active = 'Y' and rq.Denial = 'Y' ) --2/9/2016 - Combined to not allow just Active = Y items to be visible.
		
--			--2/9/2016 - This OR statement is completely new for the MGActionList table. Includes Aging claims if the max ReportDate has a priority <> Low.
--			OR (rq.Active = 'Y' 
--				and rq.Aging = 'Y' 
--				and (
--					rq.ClaimNumber in (
--						select mg.claimID  --This runs faster as a subquery than in the JOIN statements above.
--						from dbo.IHSI_MG4_ActionList mg
--							LEFT JOIN dbo.IHSI_MG4_ActionList as mg2 with(nolock) on mg.claimId = mg2.claimId  --2/9/2016 - NEW: pulls the max ReportDate
--								AND mg.ReportDate < mg2.ReportDate
--						where mg.[priority] <> 'low'
--							and mg2.[priority] is null --pulls the max ReportDate
--							)
--					OR (rq.Escalated = 'Y' OR rq.Pending = 'Y' OR rq.PendingCoding = 'Y') --4/5/2016: Added to allow the previously hidden Aging items (Aging = '') that have been Escalated/Pended and now show Aging = 'Y' to be viewable in the Escalated/Pending queues.
--					)
--				) 
--			OR (rq.Active = 'Y' and rq.Aging = '' ) --3/1/2016 - added to allow for all Aging claims to be searched, but not necessarily appear in the main queue.
--			OR (ra.ActivityEndTime >= Convert(Datetime, Convert(char(10), GetDate(), 101)) AND ra.WorkedByID = @UserID) --unchanged from logic prior to 2/9/2016
--			)
--		AND s.servicestatuscode IN ('3', '4', '23', '24', '99')
--		AND rq.QueueLock = 'N'
--		AND (rq.AssignedToID = @UserID OR rq.AssignedToID IS NULL OR rq.AssignedToID = '') -- Comment out this line if you want to run as a query
--		AND pmt2.number IS NULL
--		AND s2.number IS NULL 
--		--AND 1 = CASE --added 7/10/2012, then removed on 7/17/2012: eliminates records for secondary payors, except when the pmt payor and the clm payors are the same or if the aging is > 30 days.
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number = Pay2.Number THEN 1  
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number <> Pay2.Number and ABS(DATEDIFF(day, clm.PrintedDate, Getdate())) > 30 THEN 1 
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number <> Pay2.Number and ABS(DATEDIFF(day, clm.PrintedDate, Getdate())) < 30 THEN 2
--		--			ELSE 1
--		--		END
--	ORDER BY ClaimDate, ClaimNum

--END




--ELSE --Run this code for clients who don't use the MiddleGate process.

--BEGIN

--	SELECT s.ProviderNumber
--		, p.number as PtNum 
--		, pay.name as ClmPayorName  
--		, pay.typecode as ClmPayorType 
--		, CASE 
--			WHEN isnull(pay2.name, '') = '' --shows Aging payor's "Age of Pmt/Denial" IF available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate
--			THEN '' ELSE pay2.name 
--			END AS PmtPayorName
--		, CASE 
--			WHEN isnull(pay2.typecode, '') = '' --shows Aging payor's "Age of Pmt/Denial" IF available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate
--			THEN '' ELSE pay2.typecode
--			END AS PmtPayorType
--		, clm.number as ClaimNum
--		, NULL as Balance
--		, (CASE WHEN isnull(cr.depositnumber, '') = '' 	--shows only the Aging payor's EOB Reason, if available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate 
--			THEN '' ELSE pmt.eobReasontext END) as PmtComment
		
--		, (CASE WHEN isnull(cr.depositnumber, '') = ''  --shows only the Aging payor's Pmt Date, if available
--				or s.EnteredDate > pmt.EnteredDate
--				or clm.PrintedDate > pmt.EnteredDate 
--			THEN '' ELSE pmt.EnteredDate END) as PaymentDate  
--		, clm.PrintedDate as ClaimDate
--		, s.ServiceStatusCode
--		, (CASE  
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) <= '30' THEN 'Current'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '31' AND '60'  THEN 'Amount>30'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '61' AND '90'  THEN 'Amount>60'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) between '91' AND '120'  THEN 'Amount>90'
--			WHEN DATEDIFF(day, clm.printeddate, Getdate()) > '120' THEN 'Amount>120'
--			ELSE  'Unknown'
--		  END) as AgingBucket
--		, DATEDIFF(day, clm.printeddate, Getdate()) as AgingDays
--		, ISNULL(s.FromDate, '1900-01-01') as DateOfService
--		, ISNULL(cl.Name, '') as ClinicianName
--		, rq.Denial
--		, rq.Aging
--		, (CASE
--				WHEN rq.Aging = 'Y' AND rq.Denial = 'Y' THEN 'AD'
--				WHEN rq.Aging = 'Y' AND rq.Denial = 'N' THEN 'A'
--				WHEN rq.Aging = 'N' AND rq.Denial = 'Y' THEN 'D'
--				ELSE 'U'
--		   END) as Type
--		, rq.Pending
--		, rq.PendingCoding
--		, ra.ActivityEndTime as ActivityDate
--		, rq.Escalated
--		, rq.AssignedToID as AssignedTo
--		, ra.Comments as EscalateNote
--		, ra.WorkedByID as WorkedBy
--		, rr.Description as PendingReason
--		, pp.PolicyNumber
--		, ra.ActivityEndTime as LastWorkedDate
--	FROM dbo.IHSI_ReimbQueue as rq with(nolock) 
--		LEFT JOIN dbo.patient as p with(nolock) ON p.Number = rq.Patientnumber
--		LEFT JOIN dbo.claim as clm with(nolock) ON rq.claimnumber = clm.number
--		LEFT JOIN dbo.Service as s with(nolock) ON s.ClaimNumber = clm.number
--		LEFT JOIN dbo.service as s2 with(nolock) ON s.ClaimNumber = s2.ClaimNumber 
--			AND s.number < s2.number AND s2.ServiceStatusCode IN ('3', '4', '23', '24', '99')
--		LEFT JOIN dbo.payment as pmt with(nolock) ON pmt.ServiceNumber = s.Number 
--		LEFT JOIN dbo.payment as pmt2 with(nolock) ON pmt.servicenumber = pmt2.servicenumber 
--			AND pmt.number < pmt2.number
--		LEFT JOIN dbo.cashreceipt as cr with(nolock) ON pmt.cashreceiptnumber = cr.number
--		--LEFT JOIN dbo.payor as pay with(nolock) ON isnull(cr.payornumber, clm.payornumber) = pay.number  --the isnull replaces the case stmts in the SELECT list
--		LEFT JOIN dbo.payor as pay with(nolock) ON clm.payornumber = pay.number
--		LEFT JOIN dbo.payor as pay2 with(nolock) ON cr.payornumber = pay2.number
--		LEFT JOIN dbo.IHSI_ReimbActivity as RA with(nolock) on RA.queueID = RQ.queueID 
--			AND (ra.ActivityID IN  --Added 2010/10/25 --moved back to a join on 6/30/11 by CW: Retrieves the last activity.
--				(SELECT MAX(RA.ActivityID) as ActivityID 
--				FROM dbo.IHSI_ReimbQueue as RQ with(nolock)
--					  INNER JOIN dbo.IHSI_ReimbActivity as RA with(nolock) on RA.queueID = RQ.queueID
--					  LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA with(nolock) on RARA.ActivityID = RA.ActivityID
--				WHERE rara.ActivityID IS NOT NULL 
				
--				--OR (rara.ActivityID IS NULL  ---2012-04-26: Removed to allow the last Worked Date to pick up items with an Action vs. ones that are only viewed.
--				--	AND ra.queueID NOT IN (
--				--		SELECT RA2.queueID 
--				--		FROM dbo.IHSI_ReimbActivity as RA2 with(nolock)
--				--			INNER JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA2 with(nolock) on RARA2.ActivityID = RA2.ActivityID
--				--		GROUP BY RA2.queueID))
				
--					GROUP BY RQ.QueueID) 
--				OR ra.ActivityID IS NULL)
										
--			--OLD VERSION:  This part of the join was moved to the where clause to improve performance (MAU 2010/10/25)
--				--AND (ra.ActivityID IN
--				--(SELECT MAX(RA.ActivityID) as ActivityID --RA.WorkedByID, 
--				--	FROM dbo.IHSI_ReimbQueue as RQ with(nolock)
--				--		  INNER JOIN dbo.IHSI_ReimbActivity as RA with(nolock) on RA.queueID = RQ.queueID
--			--		  INNER JOIN dbo.IHSI_ReimbActivityReasonAssignment as RARA with(nolock) on RARA.ActivityID = RA.ActivityID
--			--		GROUP BY RQ.QueueID) OR ra.ActivityID IS NULL)											
--		LEFT JOIN dbo.IHSI_ReimbActivityReasonAssignment rar with(nolock) on RA.ActivityID = rar.ActivityID 
--			AND (rar.ReasonID IN (SELECT ReasonID FROM dbo.IHSI_ReimbReason WHERE type = 'P'))
--		LEFT JOIN dbo.IHSI_ReimbReason rr on rar.ReasonID = rr.ReasonID AND rr.Type = 'P'
--		LEFT JOIN dbo.Clinician as cl with(nolock) ON s.PerformedByID = cl.Number
--		LEFT JOIN dbo.PatientPayor pp on pp.PayorNumber = clm.PayorNumber AND pp.PatientNumber = clm.PatientNumber
--	WHERE (
--			rq.Active = 'Y' 
--			OR rq.Denial = 'Y' 
--			OR rq.Aging = 'Y'
--			OR (ra.ActivityEndTime >= Convert(Datetime, Convert(char(10), GetDate(), 101)) AND ra.WorkedByID = @UserID)
--			)
--		AND s.servicestatuscode IN ('3', '4', '23', '24', '99')
--		AND rq.QueueLock = 'N'
--		AND (rq.AssignedToID = @UserID OR rq.AssignedToID IS NULL OR rq.AssignedToID = '') -- Comment out this line if you want to run as a query
--		AND pmt2.number IS NULL
--		AND s2.number IS NULL 
--		--AND 1 = CASE --added 7/10/2012, then removed on 7/17/2012: eliminates records for secondary payors, except when the pmt payor and the clm payors are the same or if the aging is > 30 days.
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number = Pay2.Number THEN 1  
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number <> Pay2.Number and ABS(DATEDIFF(day, clm.PrintedDate, Getdate())) > 30 THEN 1 
--		--			WHEN s.ServiceStatusCode = 24 and Pay.Number <> Pay2.Number and ABS(DATEDIFF(day, clm.PrintedDate, Getdate())) < 30 THEN 2
--		--			ELSE 1
--		--		END
--	ORDER BY ClaimDate, ClaimNum

--END


--*/