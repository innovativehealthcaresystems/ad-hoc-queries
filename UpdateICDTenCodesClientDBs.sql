update tgt
set tgt.ShortText = src.shortdesc
from cain.ihs_db.dbo.ICDUpload2 src 
	left join ICDTen tgt on src.Code = tgt.Code
where Header = '1'
	and tgt.ShortText <> src.ShortDesc 
	and tgt.Type = 'C'


update tgt
set tgt.Description = src.LongDesc
from cain.ihs_db.dbo.ICDUpload2 src 
	left join ICDTen tgt on src.Code = tgt.Code
where Header = '1'
	and tgt.Description <> src.LongDesc 
	and tgt.Type = 'C'


insert into ICDTen (Code, Description, EffectiveDate, InEffectiveDate, ParentCode, ShortText, Type )
select src.Code, LongDesc, '2018-10-01', '2050-01-01 00:00:00.000', '', ShortDesc, 'C'
from cain.ihs_db.dbo.ICDUpload2 src 
	left join ICDTen tgt on src.Code = tgt.Code
	--left join ICDTen tgt on src.Code = replace(tgt.Code, '.', '')
where Header = '1'
	and tgt.Code is null

