select s.PatientNumber
	,s.Number as OldServiceNumber
	,s.FromDate as DOS
	,s.ServiceStatusCode as OldServiceStatusCode
	,s.ServiceItemCode as OldCPT
	,si.ServiceItemCode as NewCPT
	,case
		when s.ServiceItemCode < si.ServiceItemCode then 'UP'
		when s.ServiceItemCode > si.ServiceItemCode then 'DOWN'
		else 'SAME'
	end as LOS
	,sum(ac.Amount) as AmountTurnedOldService
	,si.Amount * si.Units as NewServiceCharge
	,count(so.Number) as OtherCPTS
	,count(pay.number) as ResponsesToNew
	,sum(pay.amount) + sum(ap.amount) as NetPaymentsNewService
	--,bbs.Balance as OldServiceBalance
	--,bbp.Balance as PatientBalance
--select count(*)
from service s
	left join adjustment ai
		on s.Number = ai.ServiceNumber
			and ai.AdjustmentTypeCode = '019'
	left join adjustment ac
		on s.Number = ac.ServiceNumber
			and ac.AdjustmentTypeCode in ('080','045')
	left join IHSI_BalanceByService bbs
		on s.Number = bbs.ServiceNumber
	left join IHSI_BalanceByPatient bbp
		on s.PatientNumber = bbp.PatientNumber
	left join Service si
		on s.PatientNumber = si.PatientNumber
	left join IHSI_CodingEntries ce
		on si.Number = ce.ServiceNumber
	left join Service so
		on s.PatientNumber = so.PatientNumber
			and s.Number <> so.Number
			and si.Number <> so.Number
			and so.amount * so.units > 0.01
	left join Payment pay
		on si.Number = pay.ServiceNumber
	left join Adjustment ap
		on si.Number = ap.ServiceNumber
			and ap.TransType = 'P'
			and pay.Number = ap.PaymentNumber
where ai.ServiceNumber is not null
	and ac.ServiceNumber is not null
	and si.PatientNumber is not null
	and ce.ServiceNumber is null
	and si.comment like '%corrected%'
	and si.postedperiod >= '201809'
group by s.PatientNumber
	,s.Number
	,s.FromDate
	,s.ServiceStatusCode
	,s.ServiceItemCode
	,si.ServiceItemCode
	,bbs.Balance
	,bbp.Balance	
	,si.Amount
	,si.Units
