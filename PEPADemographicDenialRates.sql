select
	x.HL7
	,x.Denial
	,count(x.Number) as ServiceCount
from
(
	select
		s.Number
		,case
			when s.FromDate <= '2018-02-01' then 'Prior'
			when s.FromDate >= '2018-03-01' then 'After'
			else 'Transition'
		end as HL7
		,case
			when pay.number is null then 'N'
			else 'Y'
		end as Denial
	from service s
	left join adjustment a
		on s.Number = a.ServiceNumber
			and a.AdjustmentTypeCode = '019'
	left join payment pay
		on s.Number = pay.ServiceNumber
			and pay.Amount = 0
	left join payment pay2
		on s.Number = pay2.ServiceNumber
			and (pay2.EnteredDate < pay.EnteredDate
				or pay2.Number < pay.Number
			)
	left join payor p
		on s.PayorNumber = p.Number
			and p.TypeCode <> 'A'
	join patient pat
		on s.PatientNumber = pat.Number
	left join ihsi_demographicimporttemplate dem
		on pat.AliasName = dem.AliasName
		and dem.Ins1PolicyNumber <> ''
		and dem.Ins1PolicyNumber is not null
	left join IHSI_DemographicImportTemplateUpdatable demup
		on pat.AliasName = demup.AliasName
		and demup.Ins1PolicyNumber <> ''
		and demup.Ins1PolicyNumber is not null
	where s.amount * s.Units > 0.01
		and s.FromDate >= '2017-09-01'
		and s.FromDate < '2018-08-01'
		and s.TransToSvcNum = ''
		and a.Number is null
		and pay2.number is null
		and ((dem.Ins1PolicyNumber is not null and dem.Ins1PolicyNumber <> '')
				or (demup.Ins1PolicyNumber is not null and demup.Ins1PolicyNumber <> ''))
	group by s.number
		,s.FromDate
		,pay.Number
)x
group by x.HL7
	,x.Denial
