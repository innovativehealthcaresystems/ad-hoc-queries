/*
-- Description:  RVU query for TX_CIP_BSW/CIPO/COP
-- Created Date:  5/01/2017 Per Rick on SR 40668. Mod comments pre creation date are for original query before it was altered for CIP/COP



-- Last Modified on 2/17/2014 by Cheryl per Rick on SR 11722:  Added the following fields to the results - Coder Number, Coder Name, Payor Type Description and Payor Name.

-- Other Mods:  
	10/10/2013 per Rick - Retrieve the Standard RVU's (ServiceItem.UnitValue) in the results.
	10/15/2013 per Rick - Add PostedPeriod to results as well as a filter.
	11/11/2013 Updated by Cheryl to add the provider number (SR 11075).
	01/31/2014 Updated by Jeff to add all service items (SR 11634).
	03/12/2015 Updated by Cheryl - Add the medical record # (MRN) to the results (SR 14470).
	03/20/2015 Updated by Cheryl - Get the Staging table's DOS field to get the time data, and if it's null, then use the Service.FromDate (SR 14520).
	03/24/2015 Updated by Cheryl - Add the Patient Name field to the results (SR 14520).
	05/01/2017 Updated by Margaret - Added logic to pull CIP specific RVUS by provider (SR 40668); lines 19-33, 44-51, 77-78
	06/02/2017 Updated by Margaret - Changed date filter to pull by Service entered date for clients with a posting period lag (SR 43114)
		also changed 015 filter to only filter for original services. It's my understanding that this query is only going to be run 
		for CIP & CIP associated DBs that have a posting period lag
	02/01/2018 Updated by Margaret (SR76727) to modify RVUs for any year latency. RVUs will be pulled for the the year the 
	RVU was active for that date of service   
	
*/

--SR 40668
declare @IHIS_Provider_RVU table ([ProviderNumber] [char](7) NULL,
	[ServiceItemCode] [char](5) NULL,
	[WorkRVU] [float] NULL,
	[TotalRVU] [float] NULL, 
	StartDate DATETIME, 
	EndDate  DATETIME)

declare @PostedPeriod varchar(6)
	
if (SELECT COUNT (*)  from INFORMATION_SCHEMA.Tables WHERE TABLE_NAME = 'IHSI_ProviderRVU') >0
begin
INSERT INTO @IHIS_Provider_RVU 
		SELECT
		R.providernumber, 
		R.serviceitemcode, 
		ISNULL (workrvu, 0.0), 
		ISNULL (totalrvu, 0.0),   
		ISNULL (StartDate, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)) AS StartDate, 
		DATEADD (DD, 1, GETDATE ())AS EndDate 
		from dbo.IHSI_ProviderRVU R 
		LEFT JOIN (select 
			providerNumber, 
			serviceItemcode, 
			MAX (ArchiveDate)AS StartDate 
			FROM dbo.IHSI_ProviderRVU_Archive
			GROUP BY ProviderNumber, 
			ServiceItemCode
			) RA 
		ON R.ProviderNumber = RA.ProviderNumber
		AND R.ServiceItemCode = RA.ServiceItemCode
		UNION 
		SELECT [ProviderNumber]
			  ,[ServiceItemCode]
			  ,[WorkRVU]
			  ,[TotalRVU]
			  ,EffectiveDate AS StartDate 
			  ,ArchiveDate AS EndDate
		  FROM [dbo].[IHSI_ProviderRVU_Archive] 
end


declare @StartEnteredDate smalldatetime = '2018-02-09'--set the start date here
declare @EndEnteredDate smalldatetime = '2018-03-09'--set the start date here
set @EndEnteredDate = DATEADD (S, 86369,  @EndEnteredDate) --this will calculate the time range on the end date for you so you don't have to worry about that 

 
select CASE
		WHEN DB_NAME() = 'TX_BSW_CIP' THEN 'CIP'
		WHEN DB_NAME() = 'TX_BSW_CIPO' THEN 'CIPO'
		WHEN DB_NAME() = 'TX_BSW_COP' THEN 'COP'
		ELSE ''
		END AS Client
	, ISNULL(NULL, 'Baylor') as Facility
	, CASE 
		WHEN DB_NAME() = 'TX_BSW_CIP' THEN 'ED'
		WHEN DB_NAME() = 'TX_BSW_CIPO' THEN 'CDU'
		WHEN DB_NAME() = 'TX_BSW_COP' THEN 'CDU'
		ELSE ''
		END AS Type
	, ISNULL(stg.TI_DateOfService, s.FromDate) AS 'DOS - TS'
	--, CAST(isnull(stg.TI_DateOfService, s.FromDate) AS DATE) as DOS --Gets the Staging DOS field to get the time data, and if it's null, then use the Service.FromDate
	, s.PatientNumber as Account
	, p.AliasName as HID
	, s.ServiceItemCode AS CPT
	, case when ipr.providerNumber is null --SR 40668
		then si.UnitValue 
		else ipr.TotalRVU 
		end as RVU
	, case when ipr.providerNumber is null 
		then si.UserFloat1 
		else ipr.WorkRVU
		end as 'RVU-Work'
	, CASE
		WHEN s.ServiceItemCode = 'ADMIT' OR
		s.ServiceItemCode = 'LTAMA' OR
		s.ServiceItemCode = 'MVACC' OR
		s.ServiceItemCode = 'NSBEP' OR
		s.ServiceItemCode = 'PAYMT' OR
		s.ServiceItemCode = 'WCOMP' OR
		s.ServiceItemCode = 'TRANS' OR
		s.ServiceItemCode = 'UNBIL' OR
		s.ServiceItemCode = 'VOID1' OR
		s.ServiceItemCode = 'SUTUR'
			THEN c.name
		WHEN s.ServiceItemCode = 'DOCUM' 
		OR s.ServiceItemCode = 'DICTA' 
		OR s.ServiceItemCode = 'ZDOCU'
		OR s.ServiceItemCode = 'LWOBS' 
			THEN 'UNKNOWN PHYSICIAN'
		WHEN c4.Name = 'MLP' OR c4.Name = '' 
			THEN 'UNKNOWN PHYSICIAN'
		ELSE c.Name
		END AS Referring
	, CASE
		WHEN s.ServiceItemCode = 'ADMIT' OR
		s.ServiceItemCode = 'LTAMA' OR
		s.ServiceItemCode = 'MVACC' OR
		s.ServiceItemCode = 'NSBEP' OR
		s.ServiceItemCode = 'PAYMT' OR
		s.ServiceItemCode = 'WCOMP' OR
		s.ServiceItemCode = 'TRANS' OR
		s.ServiceItemCode = 'UNBIL' OR
		s.ServiceItemCode = 'VOID1' OR
		s.ServiceItemCode = 'SUTUR'
			THEN c.name
		WHEN s.ServiceItemCode = 'DOCUM' 
		OR s.ServiceItemCode = 'DICTA' 
		OR s.ServiceItemCode = 'ZDOCU'
		OR s.ServiceItemCode = 'LWOBS' 
			THEN 'UNKNOWN PHYSICIAN'
		WHEN c4.Name = 'MLP' OR c4.Name = ''
			THEN 'UNKNOWN PHYSICIAN'
		ELSE c2.Name
		END AS Billing
	, CASE
		WHEN s.ServiceItemCode = 'ADMIT' OR
		s.ServiceItemCode = 'LTAMA' OR
		s.ServiceItemCode = 'MVACC' OR
		s.ServiceItemCode = 'NSBEP' OR
		s.ServiceItemCode = 'PAYMT' OR
		s.ServiceItemCode = 'WCOMP' OR
		s.ServiceItemCode = 'TRANS' OR
		s.ServiceItemCode = 'UNBIL' OR
		s.ServiceItemCode = 'VOID1' OR
		s.ServiceItemCode = 'SUTUR'
			THEN c.name
		WHEN s.ServiceItemCode = 'DOCUM' 
		OR s.ServiceItemCode = 'DICTA' 
		OR s.ServiceItemCode = 'ZDOCU'
		OR s.ServiceItemCode = 'LWOBS' 
			THEN 'UNKNOWN PHYSICIAN'
		WHEN c4.Name = 'MLP' OR c4.Name = ''
			THEN 'UNKNOWN PHYSICIAN'
		ELSE c3.Name
		END AS Servicing
	, CASE
		WHEN s.ServiceItemCode = 'ADMIT' OR
		s.ServiceItemCode = 'LTAMA' OR
		s.ServiceItemCode = 'MVACC' OR
		s.ServiceItemCode = 'NSBEP' OR
		s.ServiceItemCode = 'PAYMT' OR
		s.ServiceItemCode = 'WCOMP' OR
		s.ServiceItemCode = 'TRANS' OR
		s.ServiceItemCode = 'UNBIL' OR
		s.ServiceItemCode = 'VOID1' OR
		s.ServiceItemCode = 'SUTUR'
			THEN c.name
		WHEN s.ServiceItemCode = 'DOCUM' 
		OR s.ServiceItemCode = 'DICTA' 
		OR s.ServiceItemCode = 'ZDOCU'
		OR s.ServiceItemCode = 'LWOBS' 
			THEN 'UNKNOWN PHYSICIAN'
		WHEN c4.Name = 'MLP' 
			 OR c4.Name = '' 
			 OR c4.Name LIKE '%NP' 
			 OR c4.Name LIKE '%PA' 
			 OR c4.Name LIKE '%PA-C' 
			 OR c4.Name LIKE '%PAC'
			 OR c4.Name LIKE '%FNP'
			 OR c4.Name LIKE '%FNP-C'
			THEN 'UNKNOWN PHYSICIAN'
		ELSE c4.Name
		END AS Supervising
	, s.PostedPeriod as 'Posting Period'
	, CAST(s.EnteredDate AS DATE) AS 'Production Date'
	, s.ProviderNumber AS Provider
	, right(s.UserText2, 4) as 'Coder No.'
	, st.Name as Coder
	, pt.Description as 'Payer Type'
	, pay.Name as Payer
	, p.MedicalRecordNo as MRN
	, p.Name as PatientName
	, CASE 
		WHEN DB_NAME() = 'TX_BSW_CIP' THEN 'ED'
		WHEN DB_NAME() = 'TX_BSW_CIPO' THEN 'CDU'
		WHEN DB_NAME() = 'TX_BSW_COP' THEN 'CDU'
		ELSE ''
		END AS Department
	, CASE
		WHEN s.ServiceItemCode = 'LWOBS' OR 
		s.ServiceItemCode = 'SEDAT' OR 
		s.ServiceItemCode = 'TRANS' OR 
		s.ServiceItemCode = 'UNBIL' OR 
		s.ServiceItemCode = 'SUTUR' OR
		s.ServiceItemCode = 'ADMIT' OR
		s.ServiceItemCode = 'LTAMA' OR
		s.ServiceItemCode = 'MVACC' OR
		s.ServiceItemCode = 'NSBEP' OR
		s.ServiceItemCode = 'PAYMT' OR
		s.ServiceItemCode = 'WCOMP' OR
		s.ServiceItemCode = 'VOID1'
			THEN 'TRACKER'
		WHEN s.ServiceItemCode = 'DOCUM' 
		OR s.ServiceItemCode = 'DICTA' 
		OR s.ServiceItemCode = 'ZDOCU' 
			THEN 'PENDING'
		WHEN c3.Name LIKE '%NP' 
			 OR c3.Name LIKE '%PA' 
			 OR c3.Name LIKE '%PA-C' 
			 OR c3.Name LIKE '%PAC'
			 OR c3.Name LIKE '%FNP'
			 OR c3.Name LIKE '%FNP-C'
			THEN 'APP'
		WHEN c3.Name LIKE '%DO' OR c3.Name LIKE '%MD'
			THEN 'Physician'
		WHEN c3.Name LIKE '%NBRP'
			THEN 'Resident'
		ELSE ''
		END AS 'SVC-Type'
	, CASE
		WHEN s.ServiceItemCode = 'ADMIT' OR
			 s.ServiceItemCode = 'LTAMA' OR
			 s.ServiceItemCode = 'MVACC' OR
			 s.ServiceItemCode = 'NSBEP' OR
			 s.ServiceItemCode = 'PAYMT' OR
			 s.ServiceItemCode = 'WCOMP'
			THEN 'TRACKER'
		WHEN s.ServiceItemCode = 'DOCUM' OR
			 s.ServiceItemCode = 'DICTA' OR
			 s.ServiceItemCode = 'ZDOCU' 
			THEN 'PENDING'
		WHEN s.ServiceItemCode = 'LWOBS'
			THEN 'LWOBS'
		WHEN s.ServiceItemCode = 'SEDAT'
			THEN 'SEDAT'
		WHEN s.ServiceItemCode = 'TRANS'
			THEN 'TRANS'
		WHEN s.ServiceItemCode = 'UNBIL' OR
			 s.ServiceItemCode = 'SUTUR'
			THEN 'UNBIL'
		WHEN s.ServiceItemCode = 'VOID1'
			THEN 'VOID1'
		WHEN c3.Name LIKE '%DO' 
			 OR c3.Name LIKE '%MD' 
			 OR c3.Name LIKE '%NBRP'
			THEN 'Attested'
		WHEN (c3.Name LIKE '%NP' 
			  OR c3.Name LIKE '%PA' 
			  OR c3.Name LIKE '%PA-C' 
			  OR c3.Name LIKE '%PAC'
			  OR c3.Name LIKE '%FNP'
			  OR c3.Name LIKE '%FNP-C') 
				AND (c2.Name LIKE '%DO' OR c2.Name LIKE '%MD')
			THEN 'Attested'
		WHEN (c3.Name LIKE '%NP' 
			  OR c3.Name LIKE '%PA' 
			  OR c3.Name LIKE '%PA-C'
			  OR c3.Name LIKE '%PAC'
			  OR c3.Name LIKE '%FNP'
			  OR c3.Name LIKE '%FNP-C') 
			AND (c2.Name LIKE '%NP' 
			 OR c2.Name LIKE '%PA' 
			 OR c2.Name LIKE '%PA-C'
			 OR c2.Name LIKE '%PAC'
			 OR c2.Name LIKE '%FNP'
			 OR c2.Name LIKE '%FNP-C')
			THEN 'Co-Signed'
		ELSE ''
		END AS Attribution
	, ISNULL(NULL, '') as Filter
	, CASE WHEN ISNUMERIC(s.ServiceItemCode) = 1
		THEN CASE WHEN s.ServiceItemCode BETWEEN 99201 AND 99291 THEN 'Yes' ELSE ' ' END
	  ELSE ' '
	  END AS 'E/M'
	--, CASE
	--	WHEN CONVERT(INT, dbo.strip_nonnumeric(s.ServiceItemCode)) BETWEEN 99201 AND 99291 THEN 'Yes'
	--	ELSE ''
	--	END AS 'E/M'
	, CASE WHEN ISNUMERIC(s.ServiceItemCode) = 1
		THEN CASE WHEN s.ServiceItemCode BETWEEN 99201 AND 99291 THEN 'Yes' ELSE ' ' END
	  WHEN s2.ServiceItemCode = 'ZDOCU' OR
		s.ServiceItemCode = 'UNBIL' OR 
		s.ServiceItemCode = 'TRANS' OR
		s.ServiceItemCode = 'SUTUR' OR 		
		s.ServiceItemCode = 'LWOBS' OR 
		s.ServiceItemCode = 'DOCUM' OR
		s.ServiceItemCode = 'DICTA'
			THEN 'Yes'
		ELSE ' '
		END AS Census
	--, CASE
	--	WHEN CONVERT(INT, dbo.strip_nonnumeric(s.ServiceItemCode)) BETWEEN 99201 AND 99291 THEN 'Yes'
	--	WHEN s2.ServiceItemCode = 'ZDOCU' OR
	--	s.ServiceItemCode = 'UNBIL' OR 
	--	s.ServiceItemCode = 'TRANS' OR
	--	s.ServiceItemCode = 'SUTUR' OR 		
	--	s.ServiceItemCode = 'LWOBS' OR 
	--	s.ServiceItemCode = 'DOCUM' OR
	--	s.ServiceItemCode = 'DICTA'
	--		THEN 'Yes'
	--	ELSE ''
	--	END AS Census
	, DATENAME(MONTH, isnull(stg.TI_DateOfService, s.FromDate)) AS 'Month'
	, YEAR(isnull(stg.TI_DateOfService, s.FromDate)) as 'Year'
	, CONVERT(DATE, isnull(stg.TI_DateOfService, s.FromDate), 1) as 'DOS-TS2'
	, CONVERT(DATE, isnull(stg.TI_DateOfService, s.FromDate), 1) as DOS
	, CASE WHEN ISNUMERIC(s.ServiceItemCode) = 1
		THEN CASE WHEN s.ServiceItemCode BETWEEN 99201 AND 99291 THEN '1' ELSE ' ' END
	  ELSE ' '
	  END AS 'Patient Count'
	, s.PostedPeriod
	--, CASE
	--	WHEN CONVERT(INT, dbo.strip_nonnumeric(s.ServiceItemCode)) BETWEEN 99201 AND 99291 THEN '1'
	--	ELSE ' '
	--	END AS 'Patient Count'
	--, CASE
	--	WHEN ISNUMERIC(s.ServiceItemCode) = 1 THEN s.ServiceItemCode	--Left in
	--	WHEN ISNUMERIC(s.ServiceItemCode) = 0 THEN 0					--for future
	--	ELSE 'Something is wrong'										--testing needs					
	--	END AS 'PatientCountCheck'																
from dbo.[Service] s with(nolock)
	--inner join Service s2 on s.number = s2.number
	left join dbo.Patient p with(nolock) on p.Number = s.PatientNumber 
	left join dbo.Admission adm with(nolock) on adm.Number = s.AdmissionNumber 
	left join dbo.Adjustment adj with(nolock) on adj.ServiceNumber = s.Number and adj.AdjustmentTypeCode in ('019')
	left join dbo.Service s2 WITH(NOLOCK) ON s.number = s2.transtosvcnum --added SR43114
	left join dbo.ServiceItem si with(nolock) on si.Code = s.ServiceItemCode 
	left join dbo.Clinician c with(nolock) on c.Number = s.ReferredByID --Referring
	left join dbo.Clinician c2 with(nolock) on c2.Number = s.PerformedByID --Billing
	left join dbo.Clinician c3 with(nolock) on c3.Number = adm.AttendingClinicianID --Servicing
	left join dbo.Clinician c4 with(nolock) on c4.Number = adm.UserText2 --Supervising
	left join dbo.Staff st with(nolock) on st.Number = right(s.UserText2, 4) --Coder Name
	left join dbo.Payor pay with(nolock) on pay.Number = s.PayorNumber 
	left join dbo.IHSI_PayorType pt with(nolock) on pt.Code = pay.TypeCode 
	left join dbo.IHSI_Staging stg with(nolock) on stg.PatientNumber  = s.PatientNumber
	LEFT JOIN dbo.Provider pr WITH(NOLOCK) on pr.Number = s.ProviderNumber
	left join @IHIS_Provider_RVU ipr on s.ProviderNumber = ipr.ProviderNumber
			and s.ServiceItemCode = ipr.ServiceItemCode --SR 40668
			and s.FromDate BETWEEN ipr.StartDate AND ipr.EndDate --SR 76727
where adj.Number is null --to filter services linked to 019 adjustments
	and s2.Number is null --to filter the 015 adj type codes from causing the service to re-appear due to a payor change.  It will only report on the original service that was attached to the 015.
	--and s.Amount > '0'  -- SR#11634
	--and s.EnteredDate between @StartEnteredDate AND @EndEnteredDate
	and s.postedperiod = '201810'
group by isnull(stg.TI_DateOfService, s.FromDate)
	, s.PatientNumber 
	, p.AliasName
	, s.ServiceItemCode
	, ipr.ProviderNumber
	, ipr.TotalRVU
	, ipr.WorkRVU
	, si.UnitValue 
	, si.UserFloat1 
	, c.Name --Referring
	, c2.Name --Billing
	, c3.Name --Servicing
	, c4.Name --Supervising
	, s.PostedPeriod 
	, s.EnteredDate
	, s.ProviderNumber 
	, right(s.UserText2, 4) --Coder Number
	, st.Name 
	, pt.Description 
	, pay.Name
	, p.MedicalRecordNo
	, p.Name
	, s2.ServiceItemCode
order by s.EnteredDate,  isnull(stg.TI_DateOfService, s.FromDate), s.PatientNumber
