select p.MedicalRecordNo
	,cast(d.ServiceDate as date) as DOS
	--,d.PatientName
	,d.AliasName as HL7AliasName
	,iolol.ImageFileName
	,'EDOU' as HL7DB
	,'OLOL' as ImageFileDB
from patient p
join IHSI_DemographicImportTemplateUpdatable d
	on p.MedicalRecordNo = d.MedicalRecNo
left join IHSI_ClinImageFiles ci
	on p.MedicalRecordNo = SUBSTRING(ci.ImageFileName, CHARINDEX('MRN-', ci.ImageFileName) + 4, CHARINDEX('_CSN', ci.ImageFileName) - (CHARINDEX('MRN-', ci.ImageFileName) + 4))
left join LA_OLOL_PEPA.dbo.IHSI_ImageFiles iolol
	on d.AliasName = iolol.TI_AccountNumber
	and iolol.ImageFileName not like '%dem%'
	and iolol.ImageFileName not like '%elgb%'
	and iolol.ImageFileName not like '%nsbep%'
where ci.ImageFileName is null
	and d.ServiceDate is not null
	and d.ServiceDate >= '2018-11-06'
	and iolol.TI_AccountNumber is not null
group by p.MedicalRecordNo
	,cast(d.servicedate as date)
	--,d.PatientName
	,d.AliasName
	,iolol.ImageFileName
order by dos asc