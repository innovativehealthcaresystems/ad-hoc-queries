select *
into #tmpAdjustments
from (
	select s.PatientNumber
		,pp.PolicyNumber
		,pp.GroupNumber
		,p.Name as PtName
		,cast(p.Birthdate as date) as PtDOB
		,cast(s.FromDate as date) as DOS
		,c.BilledAmount
		,s.ServiceItemCode
		,sum(a2.Amount) as AdjustmentAmount
		--,a2.Number as AdjNum
		,max(adjt.Description) as MaxDescription
		--,pay.PayAmount + pay.AdjAmount as PaymentAmount
		--,pay.PmtNumber
	 	,s.ClaimNumber
		,c.PrintedDate
		,s.Number as ServiceNumber
	from service s
	left join Adjustment a
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = '019'
	left join Adjustment a2
		on s.Number = a2.ServiceNumber
		and a2.TransType = 'S'
	left join AdjType adjt
		on a2.AdjustmentTypeCode = adjt.Code
	join PatientPayor pp
		on s.PatientNumber = pp.PatientNumber
	join patient p
		on s.PatientNumber = p.Number
	join Claim c
		on s.ClaimNumber = c.Number
		and pp.PayorNumber = c.PayorNumber
	where s.FromDate >= '2018-01-01'
		--and s.FromDate < '2019-01-01'
		and left(pp.PolicyNumber, 3) = 'XOG'
		and s.TransToSvcNum = ''
		and a.ServiceNumber is null
		and left(s.ServiceItemCode, 2) = '99'
	
		--and s.PatientNumber = '000001005'
		--and cast(s.FromDate as date) = '2018-01-22'
	
	group by s.PatientNumber
		,pp.PolicyNumber
		,p.Name
		,cast(p.Birthdate as date)
		,cast(s.FromDate as date)
		,c.BilledAmount
		,s.ServiceItemCode
	 	,s.ClaimNumber
		--,a2.Amount
		--,a2.Number
		----,pay.PmtNumber
		----,pay.PayAmount
		----,pay.AdjAmount
		--,adjt.Description
		,s.Number
		,pp.GroupNumber
		,c.PrintedDate
	--order by s.PatientNumber asc
		--,dos asc
)A

--select *
--from #tmpAdjustments ta
--where ta.PatientNumber = '000017786'
--	and ta.DOS = '2019-01-04'

select *
into #tmpTotalPayments
from (
	select db_name() as DatabaseName
		,ta.PatientNumber
		,ta.PolicyNumber
		,ta.GroupNumber
		,ta.PtName
		,ta.PtDOB
		,ta.DOS
		,ta.BilledAmount
		,ta.ServiceItemCode
		,ta.AdjustmentAmount
		,ta.MaxDescription
		,ta.ClaimNumber
		,cast(ta.PrintedDate as date) as ClaimPrintedDate
		,isnull(sum(pay.PayAmount + pay.AdjAmount), 0) as TotalInsPayments
		,cast(max(pay.PmtEntDate) as date) as LastPaymentPostedDate

		,ta.ServiceNumber
	----select *
	from #tmpAdjustments ta
	left join IHSI_MGTPayments pay
		on ta.ServiceNumber = pay.svcnumber
		and pay.pmtpayor <> '0000001'
	--where ta.PatientNumber = '000017786'
	----	and ta.DOS = '2019-01-04'
	group by ta.PatientNumber
		,ta.PolicyNumber
		,ta.PtName
		,ta.PtDOB
		,ta.DOS
		,ta.BilledAmount
		,ta.ServiceItemCode
		,ta.AdjustmentAmount
		,ta.MaxDescription
		,ta.ClaimNumber
		,ta.GroupNumber
		,ta.PrintedDate
		,ta.ServiceNumber
	--order by PatientNumber asc
	--	,dos asc
)B

select *
into #tmpPriorPayments
from (
	select ttp.ServiceNumber
		,isnull(sum(payold.PayAmount + payold.AdjAmount), 0) as InsPaymentsPriorToMarch20
	from #tmpTotalPayments ttp
	left join IHSI_MGTPayments payold
		on ttp.ServiceNumber = payold.svcnumber
		and payold.pmtpayor <> '0000001'
		and payold.PmtEntDate < '2019-03-20'
	group by ttp.ServiceNumber
)C

select *
into #tmpPostPayments
from (
	select tpp.ServiceNumber
		,isnull(sum(paynew.PayAmount + paynew.AdjAmount), 0) as AdditionalInsPayments
	from #tmpPriorPayments tpp
	left join IHSI_MGTPayments paynew
		on tpp.ServiceNumber = paynew.svcnumber
		and paynew.pmtpayor <> '0000001'
		and paynew.PmtEntDate >= '2019-03-20'
	group by tpp.ServiceNumber
)D

select distinct ttot.DatabaseName
	,ttot.PatientNumber
	,ttot.PolicyNumber
	,ttot.GroupNumber
	,ttot.PtName
	,ttot.PtDOB
	,ttot.DOS
	,ttot.BilledAmount
	,ttot.ServiceItemCode
	,ttot.AdjustmentAmount
	,ttot.MaxDescription
	,ttot.ClaimNumber	
	,ttot.ClaimPrintedDate
	,tprior.InsPaymentsPriorToMarch20
	,tpost.AdditionalInsPayments
	,ttot.TotalInsPayments
	,ttot.LastPaymentPostedDate
from #tmpTotalPayments ttot
left join #tmpPriorPayments tprior
	on ttot.ServiceNumber = tprior.ServiceNumber
left join #tmpPostPayments tpost
	on ttot.ServiceNumber = tpost.ServiceNumber

drop table #tmpPostPayments
drop table #tmpPriorPayments
drop table #tmpTotalPayments
drop table #tmpAdjustments