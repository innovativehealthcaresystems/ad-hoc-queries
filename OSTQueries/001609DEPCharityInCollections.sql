select RIGHT('000000000'+CAST(tc.PatientNumber AS VARCHAR(9)),9) as DEPIHSIPatientNumber
	,p.Number as GRMPatientNumber
	,s.UserText1 as ClientAccountNumber
	,s.ServiceStatusCode
	,s.Amount * s.Units as AmountTurnedToGRM
	,sum(a.amount) as AmountTurnedToMBB
	,s.EnteredDate as TurnedDate
from TurnedCharity tc
join patient p
	on RIGHT('000000000'+CAST(tc.PatientNumber AS VARCHAR(9)),9) = p.AliasName
join Service s
	on p.Number = s.PatientNumber
	and tc.AcctNo = s.UserText1
left join Adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '850'
group by tc.patientnumber
	,p.number
	,s.usertext1
	,s.servicestatuscode
	,s.amount
	,s.units
	,s.EnteredDate

drop table TurnedCharity