select * 
into #tmpStep1
from
(
	select
		cl.ID
		,vl.DatabaseNumber
		,ac.CpnyName
		,cd.DispositionID
		,d.Description
		,datepart(year, cl.StartTime) as [Year]
		,DATENAME(month, cl.StartTime) as [Month]
		,datepart(month, cl.StartTime) as MonthNum
		,case
			when cd.DispositionID = 80 then 1
			else 0
		end as Trans
		,case
			when cd.DispositionID <> 80 then 1
			else 0
		end as Other
	from PSR_CallLog cl
	left join PSR_PatientVerificationLog vl
		on cl.ID = vl.CallID
	left join PSR_CallDisposition cd
		on vl.ID = cd.VerifyID
	left join PSR_Dispositions d
		on cd.DispositionID = d.RespCode
	left join IHS_DB.dbo.IHSI_ActiveClients ac
		on vl.DatabaseNumber = ac.CpnyID
	where 
		cl.StartTime >= '2018-01-01'
		and cl.StartTime < '2019-01-01'
		and vl.DatabaseNumber in ('387','392','393')
) TS1

select TS1.CpnyName
	,TS1.DatabaseNumber
	,TS1.Year
	,TS1.Month
	,sum(TS1.Trans) as 'Transferred to Third Party'
	,sum(Other) as 'All Other Dispositions'
from #tmpStep1 TS1
group by TS1.CpnyName
	,TS1.DatabaseNumber
	,TS1.Year
	,TS1.Month
	,TS1.MonthNum
order by TS1.DatabaseNumber asc
	,TS1.MonthNum asc

drop table #tmpStep1