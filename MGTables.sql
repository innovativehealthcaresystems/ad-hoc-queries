select top 1 c.PrintedDate
	,*
from IHSI_EDI_ClaimLevelAdjustments cla
join Claim c
	on cla.ClaimID = c.Number
order by c.PrintedDate desc

select top 1 StatementBegin
	,*
from IHSI_EDI_PaidClaims pc
order by id desc

select top 1 cp.FileDate
	,*
from IHSI_EDI_ClaimPayment cp
order by cp.FileDate desc

select top 1 *
from IHSI_MG_X12_835_Journal mj
order by mj.InsertDate desc

select top 1 *
from IHSI_MG_ClaimsJournal cj
order by cj.InsertDate desc

select top 1 *
from IHSI_MG_ServiceStatus_Journal ssj
order by ssj.UpdateDate desc