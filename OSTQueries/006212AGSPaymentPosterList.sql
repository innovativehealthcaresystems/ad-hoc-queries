select *
from staff s
join cain.icis.dbo.users u
	on s.Number = u.solomonid
join cain.icis.dbo.usergroups ug
	on u.userid = ug.userid
where s.Active = 'Y'
	and LEFT(s.Number, 1) = 'A'	--AGS user prefix
	and s.DepartmentCode in	('I08')	--Reimbursements seemed too much, may be used for RID	--('I08', 'I10')	--Cash Receipts and Reimbursements Respectively
	and u.active = 1
order by s.LastUpdatedDate desc

/*Refund group id is 142*/