/*
-- Description:  RVU query for TX_JPS_IES
-- Created Date:  10/9/2013 Per Rick on SR 10189 

-- Last Modified on 2/17/2014 by Cheryl per Rick on SR 11722:  Added the following fields to the results - Coder Number, Coder Name, Payor Type Description and Payor Name.

-- Other Mods:  
	10/10/2013 per Rick - Retrieve the Standard RVU's (ServiceItem.UnitValue) in the results.
	10/15/2013 per Rick - Add PostedPeriod to results as well as a filter.
	11/11/2013 Updated by Cheryl to add the provider number (SR 11075).
	01/31/2014 Updated by Jeff to add all service items (SR 11634).
	03/12/2015 Updated by Cheryl - Add the medical record # (MRN) to the results (SR 14470).
	03/20/2015 Updated by Cheryl - Get the Staging table's DOS field to get the time data, and if it's null, then use the Service.FromDate (SR 14520).
	03/24/2015 Updated by Cheryl - Add the Patient Name field to the results (SR 14520).
*/


declare @IHIS_RVU table (
	[ServiceItemCode] [char](5) NULL,
	[WorkRVU] [float] NULL,
	[TotalRVU] [float] NULL, 
	StartDate DATETIME, 
	EndDate  DATETIME)

	INSERT INTO @IHIS_RVU 
		SELECT
		R.code, 
		ISNULL (UserFloat1, 0.0), 
		ISNULL (UnitValue, 0.0),   
		ISNULL (StartDate, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)) AS StartDate, 
		DATEADD (DD, 1, GETDATE ())AS EndDate 
		from dbo.ServiceItem R 
		LEFT JOIN (select 
			serviceItemcode, 
			MAX (ArchiveDate)AS StartDate 
			FROM dbo.IHSI_RVU_Archive
			GROUP BY 
			ServiceItemCode
			) RA 
		ON R.Code = RA.ServiceItemCode
		UNION 
		SELECT 
			  [ServiceItemCode]
			  ,ISNULL (UserFloat1, 0.0) 
			  ,ISNULL (UnitValue, 0.0)  
			  ,EffectiveDate AS StartDate 
			  ,ArchiveDate AS EndDate
		  FROM [dbo].[IHSI_RVU_Archive] 




declare @PostedPeriod char(6)
set @PostedPeriod = '201809'  --Change Posted Period here
 
select isnull(stg.TI_DateOfService, s.FromDate) as DOS --Gets the Staging DOS field to get the time data, and if it's null, then use the Service.FromDate
	, s.PatientNumber as IHSI_Account
	, p.AliasName as HID
	, s.ServiceItemCode
	, ISNULL (si.TotalRVU, si2.UnitValue)  as StdRVU
	, ISNULL (si.WorkRVU, si2.UserFloat1)  as WorkRVU
	, c.Name as Referring
	, c2.Name as Billing
	,case
		when c3.name is null then c5.name
		else c3.name
	end as Servicing
	,case
		when c4.Name is null then c6.name
		else c4.name
	end as Supervising
	, s.PostedPeriod 
	, s.ProviderNumber 
	, right(s.UserText2, 4) as CoderNumber
	, st.Name as CoderName
	, pt.Description as PayorTypeDescription
	, pay.Name as PayorName
	, p.MedicalRecordNo as MRN
	, p.Name as PatientName
from dbo.service s with(nolock)
	left join dbo.Patient p with(nolock) on p.Number = s.PatientNumber 
	left join dbo.Admission adm with(nolock) on adm.Number = s.AdmissionNumber 
	left join dbo.Adjustment adj with(nolock) on adj.ServiceNumber = s.Number and adj.AdjustmentTypeCode in ('015', '019')
	--left join dbo.ServiceItem si with(nolock) on si.Code = s.ServiceItemCode 
	left join @IHIS_RVU si ON si.ServiceItemCode = S.ServiceItemCode
		AND s.FromDate BETWEEN si.StartDate AND si.EndDate 
	left join dbo.ServiceItem si2 ON s.ServiceItemCode = si2.Code 	
	left join dbo.Clinician c with(nolock) on c.Number = s.ReferredByID --Referring
	left join dbo.Clinician c2 with(nolock) on c2.Number = s.PerformedByID --Billing
	left join dbo.Clinician c3 with(nolock) on c3.Number = adm.AttendingClinicianID --Servicing
	left join dbo.Clinician c4 with(nolock) on c4.Number = adm.UserText2 --Supervising
	left join dbo.Staff st with(nolock) on st.Number = right(s.UserText2, 4) --Coder Name
	left join dbo.Payor pay with(nolock) on pay.Number = s.PayorNumber 
	left join dbo.IHSI_PayorType pt with(nolock) on pt.Code = pay.TypeCode 
	left join dbo.IHSI_Staging stg with(nolock) on stg.PatientNumber  = s.PatientNumber 

	left join service s1 on s.PatientNumber = s1.PatientNumber
	join admission a1 on s1.AdmissionNumber = a1.Number
	left join Clinician c5 on c5.Number = a1.AttendingClinicianID -- Servicing
	left join Clinician c6 on c6.Number = a1.UserText2 --Supervising
where adj.Number is null --to filter services linked to 015 and 019 adjustments
	--and s.Amount > '0'  -- SR#11634
	and s.PostedPeriod = @PostedPeriod 
group by isnull(stg.TI_DateOfService, s.FromDate)
	, s.PatientNumber 
	, p.AliasName
	, s.ServiceItemCode
	, ISNULL (si.TotalRVU, si2.UnitValue)
	, ISNULL (si.WorkRVU, si2.UserFloat1)
	, c.Name --Referring
	, c2.Name --Billing
	, c3.Name --Servicing
	, c4.Name --Supervising
	, s.PostedPeriod 
	, s.ProviderNumber 
	, right(s.UserText2, 4) --Coder Number
	, st.Name 
	, pt.Description 
	, pay.Name
	, p.MedicalRecordNo
	, p.Name
	--	, a1.AttendingClinicianID
	--, a1.UserText2
	,c5.Name
	,c6.Name
order by isnull(stg.TI_DateOfService, s.FromDate), s.PatientNumber








