select
	s.ServiceStatusCode
	,ssc.Description
	,s.EnteredDate
	,sum(s.Amount * s.Units) as Charges
	,s.ProviderNumber
	,p.OrganizationName
	,s.PayorNumber
	,pay.Name
	,s.PatientNumber
	,case
		when pc.patientnumber is not null then 'Y'
		else 'N'
	end as PatientCalled
from service s
join IHSI_UHCPayorHolds uph
	on s.PayorNumber = uph.PayorNumber
join IHSI_ServiceStatusCodes ssc
	on s.ServiceStatusCode = ssc.Code
join Provider p
	on s.ProviderNumber = p.Number
join Payor pay
	on s.PayorNumber = pay.Number
left join IHSI_UHCPatientsCalled pc
	on s.PatientNumber = pc.patientnumber
where s.EnteredDate >= '2018-09-01'
	and s.ProviderNumber = '0000003'
group by
	s.ServiceStatusCode
	,ssc.Description
	,s.EnteredDate
	,s.ProviderNumber
	,s.PayorNumber
	,pay.Name
	,s.PatientNumber
	,p.OrganizationName
	,pc.patientnumber