/*
1. Adjust ED services not in collections
1.5 Update service status codes
2. Send ED services in collections to Sandra/Sharon for manual review
3. Send Non-ED acocunts to Jennifer/PAD for manual adjustment
4. Mark everything that has been handled as complete. Leave unbilled ED patients alone for next time
*/

/*Pull accounts that haven't been processed(imported) yet and now have a billable service on them*/
select *
into #tmpPatients
from
(
	select c.AcctNo
		,cast(left(c.AdmitDate, 10) as date) as AdmitDate
		,p.Number as PatientNumber
		,cast(s.FromDate as date) as FromDate
		,case
			when sum(a.amount) is null then 'N'
			else 'Y'
		end as Collections
		,max(pyr.TypeCode) as MaxTypeCode
			---- as AmountInCollections
	from IHSI_DEPCharity c
	join patient p
		on c.AcctNo = p.AliasName
	join service s
		on p.Number = s.PatientNumber
		and s.Amount > 0.01
	left join adjustment a	--Accounts that have been in collections need to go to Sharon/Sandra to look at and manually handle.
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode in ('080','045')
	left join Payment pmt
		on s.Number = pmt.ServiceNumber
	left join CashReceipt cr
		on pmt.CashReceiptNumber = cr.Number
	left join Payor pyr	--Flag accounts with patient payments to be worked manually (refunds needed)
		on cr.PayorNumber = pyr.Number
		and pyr.TypeCode = 'A'
	where c.OriginalHospitalService = 'EEM'
		and Abs(datediff(day, cast(left(c.AdmitDate, 10) as date), cast(s.FromDate as date))) < 7 --Make sure their DOS is	somewhat close to ours
		and c.Imported = 'N'	----Removed 1/18/19 by Justin for testing
	group by c.AcctNo
		,cast(left(c.AdmitDate, 10) as date)
		,p.Number
		,cast(s.FromDate as date)
) as A

/*Pull services that have a positive balance, haven't turned to collections and don't have any pt payments; and adjust them automatically*/
select *
into #tmpAdjustments
from
(
	select tp.AcctNo
		,tp.PatientNumber
		,s.Number as ServiceNumber
		,s.ServiceStatusCode
		,s.LastUpdatedBy
		,s.LastUpdatedDate
		,s.FromDate
		,s.Amount * s.Units as ServiceCharge
		,bbs.Balance as ServiceBalance
	from #tmpPatients tp
	join service s
		on tp.PatientNumber = s.PatientNumber
		and s.Amount > 0
	join IHSI_BalanceByService bbs
		on s.Number = bbs.ServiceNumber
	where tp.Collections = 'N'
		and bbs.Balance > 0
		and ISNULL(tp.MaxTypeCode, 'Z') <> 'A'
	group by tp.AcctNo
		,tp.PatientNumber
		,s.Number
		,tp.Collections
		,s.ServiceStatusCode
		,s.LastUpdatedBy
		,s.LastUpdatedDate
		,s.FromDate
		,bbs.Balance 
		,s.Amount
		,s.Units
) as B

alter table #tmpAdjustments
add RowID smallint identity
	,AdjNumber varchar(9)

DECLARE @NextAdjustmentNumber int	--Run adjustments
SET @NextAdjustmentNumber = (SELECT [NextNumber] FROM dbo.AdjustmentSequence)

UPDATE #tmpAdjustments
SET AdjNumber = RIGHT(RTRIM('000000000' + CAST((RowID) + @NextAdjustmentNumber AS VARCHAR)), 9)

INSERT INTO dbo.Adjustment (AdjReasonCategory, AdjReasonCode, AdjustmentTypeCode, Amount, EnteredByID, 
	EnteredDate, Number, PaymentNumber, PayorNumber, Posted, PostedPeriod, PrintOnStmtFlag, ReasonText, 
	ServiceNumber, StatementNumber, SubAccount, TransType, UserDate1, UserDate2, UserFloat1, UserFloat2, UserText1,
	UserText2, UserText3, UserText4)
SELECT '' AS AdjReasonCategory
	,'' AS AdjReasonCode 
	,'142' AS AdjustmentTypeCode
	,-1*ServiceBalance AS Amount
	,'9999' AS EnteredByID
	,getdate() AS EnteredDate
	,AdjNumber AS Number
	,'' AS PaymentNumber
	,'' AS PayorNumber
	,'Y' AS Posted
	,(SELECT CurrentPeriodNumber FROM dbo.mbsetup) AS PostedPeriod
	,'Y' AS PrintOnStmtFlag
	,'CON ADJ - CHARITY APPROVED' AS ReasonText
	,ServiceNumber as ServiceNumber
	,'' AS StatementNumber
	,'' AS SubAccount
	,'S' as TransType
	,getdate() AS UserDate1
	,'1900-01-01 00:00:00' as UserDate2
	,0 as UserFloat1
	,0 as UserFloat2
	,'' as UserText1
	,'' as UserText2
	,'' as UserText3
	,'' as UserText4
FROM #tmpAdjustments

UPDATE [dbo].[AdjustmentSequence]
SET [NextNumber] = (SELECT MAX(RIGHT(RTRIM('000000000' + Number), 9)) + 1 FROM Adjustment)

/*Update service status codes*/
update s
set s.ServiceStatusCode	= 40
	,s.LastUpdatedDate = GETDATE()
	,s.LastUpdatedBy = '9999'
from #tmpAdjustments ta
join service s
	on ta.ServiceNumber = s.Number

/*Pull accounts for manual review by Sharon/Sandra*/
select *
from #tmpPatients tp
where tp.Collections = 'Y'

/*Pull non-ED account for Jennifer to work*/
select *
from IHSI_DEPCharity dc
where dc.OriginalHospitalService <> 'EEM'
	and dc.Imported = 'N'	--Commented 1/21/19 for testing JDT

/*Pull PT Pmt accounts for Jennifer to work manually*/
select *
from #tmpPatients tp
where ISNULL(tp.MaxTypeCode, 'Z') = 'A'
	and tp.Collections = 'N'


/*Mark accounts as worked (set imported = 'Y')*/
update dc
set dc.Imported = 'Y'
from #tmpPatients tp
join patient p
	on tp.PatientNumber = p.Number
join IHSI_DEPCharity dc
	on p.AliasName = dc.AcctNo

update dc
set dc.Imported = 'Y'
from IHSI_DEPCharity dc
where dc.OriginalHospitalService <> 'EEM'
	and dc.Imported = 'N'
	
--select *
--from #tmpAdjustments

--select *
--from IHSI_DEPCharity
--where Imported = 'N'

drop table #tmpPatients
drop table #tmpAdjustments

