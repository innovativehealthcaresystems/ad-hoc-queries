create table #tmpCPTRates (
	CPT char(5)
	,ReimbRate float
	,AdjustmentAmount float
)

insert into #tmpCPTRates values

	('99281', 22.04, 204.96)
	,('99282', 43.03, 261.97)
	,('99283', 64.35, 394.65)
	,('99284', 122.13, 562.87)
	,('99285', 179.90, 842.10)
	,('99291', 230.37, 1270.63)

,('99201', 26.53, 158.47)
,('99202', 49.49, 180.51)
,('99203', 74.95, 225.05)
,('99204', 126.10, 303.90)
,('99205', 164.44, 420.56)
,('99211', 8.86, 96.14)
,('99212', 24.90, 125.10)
,('99213', 49.50, 140.50)
,('99214', 76.08, 203.92)
,('99215', 107.53, 337.47)

select *
into #tmpMain
from (
	select s.PatientNumber
		,pp.PolicyNumber
		,p.Name as PtName
		,cast(p.Birthdate as date) as PtDOB
		,cast(s.FromDate as date) as DOS
		,c.BilledAmount
		,s.ServiceItemCode
		,tcr.ReimbRate
		,tcr.AdjustmentAmount as AllowedAdjustment
		,min(a2.Amount) as ActualAdjustment
		,pay.pmtpayor
		,sum(pay.PayAmount + pay.AdjAmount) as InsPayments
		,min(adjt.Description) as Description
		,s.ClaimNumber
		,s.Number as ServiceNumber
	from service s
	join #tmpCPTRates tcr
		on s.ServiceItemCode = tcr.CPT
	join PatientPayor pp
		on s.PatientNumber = pp.PatientNumber
	left join Adjustment a
		on s.Number = a.ServiceNumber
		and -1 * tcr.AdjustmentAmount = a.Amount
	left join Adjustment a2
		on s.Number = a2.ServiceNumber
		and a2.Amount < -1 * tcr.AdjustmentAmount
	join patient p
		on s.PatientNumber = p.Number
	join AdjType adjt
		on a2.AdjustmentTypeCode = adjt.Code
	join Claim c
		on s.ClaimNumber = c.Number
		and pp.PayorNumber = c.PayorNumber
	left join IHSI_MGTPayments pay
		on s.Number = pay.svcnumber
		and pay.pmtpayor <> '0000001'
	where s.FromDate >= '2018-01-01'
		--and s.FromDate < '2019-01-01'
		and left(pp.PolicyNumber, 3) = 'XOG'
		and a.ServiceNumber is null
		and a2.ServiceNumber is not null
		and s.TransToSvcNum = ''
	group by s.PatientNumber
		,pp.PolicyNumber
		,p.Name 
		,cast(p.Birthdate as date)
		,cast(s.FromDate as date)
		,c.BilledAmount
		,s.ServiceItemCode
		,tcr.ReimbRate
		,tcr.AdjustmentAmount
		--,a2.Amount
		,pay.pmtpayor
		--,adjt.Description
		,s.ClaimNumber
		,s.Number
	--order by s.PatientNumber asc
)A

select *
into #tmpExtras
from (
	select s.PatientNumber
		,pp.PolicyNumber
		,p.Name as PtName
		,cast(p.Birthdate as date) as PtDOB
		,cast(s.FromDate as date) as DOS
		,c.BilledAmount
		,s.ServiceItemCode
		,0 as ReimbRate
		,0 as AllowedAdjustment
		,0 as ActualAdjustment
		,pay.pmtpayor
		,sum(pay.PayAmount + pay.AdjAmount) as InsPayments
		,'' as Description
		,s.ClaimNumber
		,s.Number as ServiceNumber
	from service s
	left join #tmpMain tm
		on s.PatientNumber = tm.PatientNumber
		and s.ClaimNumber = tm.ClaimNumber
	--join #tmpCPTRates tcr
	--	on s.ServiceItemCode = tcr.CPT
	join PatientPayor pp
		on s.PatientNumber = pp.PatientNumber
	--left join Adjustment a
	--	on s.Number = a.ServiceNumber
	--	and -1 * tcr.AdjustmentAmount = a.Amount
	--left join Adjustment a2
	--	on s.Number = a2.ServiceNumber
	--	and a2.Amount < -1 * tcr.AdjustmentAmount
	join patient p
		on s.PatientNumber = p.Number
	--join AdjType adjt
	--	on a2.AdjustmentTypeCode = adjt.Code
	join Claim c
		on s.ClaimNumber = c.Number
		and pp.PayorNumber = c.PayorNumber
	left join IHSI_MGTPayments pay
		on s.Number = pay.svcnumber
		and pay.pmtpayor <> '0000001'
	where s.FromDate >= '2018-01-01'
		--and s.FromDate < '2019-01-01'
		and left(pp.PolicyNumber, 3) = 'XOG'
		--and a.ServiceNumber is null
		--and a2.ServiceNumber is not null
		and s.TransToSvcNum = ''
		and s.Number <> tm.ServiceNumber
		and left(s.ServiceItemCode, 2) = '99'
		and s.ServiceItemCode <> tm.ServiceItemCode
	group by s.PatientNumber
		,pp.PolicyNumber
		,p.Name 
		,cast(p.Birthdate as date)
		,cast(s.FromDate as date)
		,c.BilledAmount
		,s.ServiceItemCode
		--,tcr.ReimbRate
		--,tcr.AdjustmentAmount
		--,a2.Amount
		,pay.pmtpayor
		--,adjt.Description
		,s.ClaimNumber
		,s.Number
)B

select *
from (
	select *
	from #tmpMain
	
	union
	
	select *
	from #tmpExtras
	) sub
order by sub.PatientNumber asc

drop table #tmpExtras
drop table #tmpMain
drop table #tmpCPTRates