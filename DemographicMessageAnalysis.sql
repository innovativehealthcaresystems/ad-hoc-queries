select count(distinct s.PatientNumber)
	,cast(datepart(year,dem.hl7messagetime) as char(4)) + cast(DATEPART(month,dem.hl7messagetime) as char(2)) as MessageMonth
	--,dem.Ins1Address1
	--,dem.Ins1Address2
	--,dem.Ins1PolicyNumber
	--,dem.Ins2PolicyNumber
from service s
	join patient p
		on s.PatientNumber = p.Number
	left join IHSI_DemographicImportTemplateUpdatable dem
		on p.AliasName = dem.AliasName
			and (dem.Ins1PolicyNumber not in ('', '00000000000')
				or dem.Ins2PolicyNumber not in ('', '00000000000')
				or Ins1Address1 <> '1701 OAK PARK BLVD')
where s.PayorNumber = '0000001'
	and s.amount * s.Units > 0.01
	and s.TransToSvcNum = ''
	and dem.AliasName is not null
	and Ins1Address1 not like '%must%'
group by cast(datepart(year,dem.hl7messagetime) as char(4)) + cast(DATEPART(month,dem.hl7messagetime) as char(2))
	--,dem.Ins1Address1
	--,dem.Ins1Address2
	--,dem.Ins1PolicyNumber
	--,dem.Ins2PolicyNumber
order by MessageMonth desc