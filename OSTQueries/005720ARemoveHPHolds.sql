ALTER procedure [dbo].[IHSI_HoldHarvardPilgrimStatements] as

/*Created 1/21/19 by Justin per Trish on OS Ticket #005720 https://tickets.ihsi.com/scp/tickets.php?id=101818.
The client requested that beginning on this day for DOS on or after 1/1/2019 we put all services with Harvard Pilgrim (payor type N) on hold after the insurance pays. Thus this will be run right after the conversion step for both Seacoast databases.

Modifications:
	- 1/30/2019 by Justin. Blocked 15 patients manually (out of 16 so far) that Krissy has ID'd as good to go.

The client plans to review and see how the insurance is paying on 2/1/2019.
*/

update s
set s.Comment =
	case
		when s.Comment = '' then 'HOLD FROM PT PER CLIENT REQUEST'
		else s.Comment end
	,s.LastUpdatedBy = '9999'
	,s.LastUpdatedDate = GETDATE()
	,s.UserText3 = 'Pyr Type N'
	,s.ServiceStatusCode = 99
from service s
join payor p
	on s.PayorNumber = p.Number
where p.TypeCode = 'N'
	and s.ServiceStatusCode in (7, 27)
	and s.FromDate >= '2019-01-01'
	and s.PatientNumber not in (
'000532504'	--Cleared	--Added 1/30/19 by Justin per Krissy. 15 of the patients are cleared
,'000532518' --Cleared
,'000532555' --Cleared
,'000532652' --Cleared
,'000532653' --Cleared
,'000532712' --Cleared
,'000532759' --Cleared
,'000532813' --Cleared
,'000532821' --Cleared
,'000532888' --Cleared
,'000532943' --Cleared
,'000532974' --Cleared
,'000533072' --Cleared
,'000533179' --Cleared
,'000533290' --Cleared
--,'000535154'--Pending
)