	select distinct p.Number as PatientNumber
		,s.Number as ServiceNumber
		,p.AddressLine1	--Review Addresses Before Writing Off
		,p.AddressLine2
		,p.city
		,p.State
		,left(p.ZipCode, 5) + '-' + right(p.ZipCode, 4) as ZipCode
		,s.ServiceStatusCode
		,bbs.Balance
		,cast(s.fromdate as date) as DOS
		,case when s.FromDate < DATEADD(month, -6, getdate()) then 'Y' else 'N' end as Over6Months
 	from patient p
	left join service s
		on p.Number = s.PatientNumber
		and s.TransToSvcNum = ''
		and s.ServiceStatusCode in (7, 27)
	left join payor pay
		on s.PayorNumber = pay.Number
	left join IHSI_BalanceByPatient bbp
		on p.Number = bbp.PatientNumber
	left join IHSI_BalanceByService bbs
		on s.Number = bbs.ServiceNumber
	where (p.AddressLine1 like 'unk%'
			or p.AddressLine2 like 'ret%'
			or p.addressline1 like '%bad%add%'
			or p.addressline2 like 'bad%add%'
			or p.AddressLine1 like '%know%')
			and s.PatientNumber is not null
			--and s.FromDate < dateadd(month, -6, getdate())
			and bbs.Balance > 0