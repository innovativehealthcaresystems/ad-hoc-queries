select 
	sold.PatientNumber
	,sold.Number
	,sold.Units * sold.Amount as Charge
	,sold.ServiceStatusCode
	,sum(acol.Amount) as AmountInCollections
from service snew
left join IHSI_CodingEntries ce
	on snew.number = ce.servicenumber
left join Service sold
	on snew.PatientNumber = sold.PatientNumber
	and sold.Comment not like '%corrected%'
	and sold.TransToSvcNum = ''
left join Adjustment acol
	on sold.Number = acol.ServiceNumber
	and acol.AdjustmentTypeCode = '080'
	and acol.TransType = 'S'
where snew.postedperiod >= '201809'
	and snew.comment like '%corrected%'
	and acol.ServiceNumber is not null
group by sold.PatientNumber
	,sold.Number
	,sold.Units  
	,sold.Amount
	,sold.ServiceStatusCode