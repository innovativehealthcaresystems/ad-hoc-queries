select pay.Number as PayorNumber
	,pay.Name as PayorName
	,c.PatientNumber
	,c.Number as ClaimNumber
	,pp.PolicyNumber
	,p.Name as PatientName
	,cast(p.Birthdate as date) as DOB
	,cast(c.NewestServiceDate as date) as DOS
	,c.BilledAmount
from payor pay
join Claim c
	on pay.Number = c.PayorNumber
join PatientPayor pp
	on c.PatientNumber = pp.PatientNumber
	and c.PayorNumber = pp.PayorNumber
	and left(pp.PolicyNumber, 1) = '1'
join Patient p
	on c.PatientNumber = p.Number
where pay.Name like '%Aetna%'