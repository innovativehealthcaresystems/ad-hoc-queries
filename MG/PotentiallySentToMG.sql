/****** Script for SelectTopNRows command from SSMS  ******/
SELECT count([Number]) as SentPatientCount
      ,cast([InsertDate] as date) as InsertDate
  FROM [LA_OLOL_PEPA].[dbo].[IHSI_MG_ClaimsJournal]
  group by cast([InsertDate] as date)
  order by InsertDate desc