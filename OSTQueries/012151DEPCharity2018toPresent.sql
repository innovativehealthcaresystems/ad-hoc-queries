select *
into #tmpCases
from (
	select s.ProviderNumber
		,pro.OrganizationName
		,p.Number as PatientNumber
		,p.AliasName as ClientAccountNumber
		,p.MedicalRecordNo
		,min(cast(s.fromdate as date)) as AdmitDate
		,sum(a.Amount) as CharityAmount
	from Adjustment a
	join service s
		on a.ServiceNumber = s.Number
		and a.AdjustmentTypeCode in ('075', '142')
		and s.FromDate >= '2018-01-01'
	join patient p
		on s.PatientNumber = p.Number
	join Provider pro
		on s.ProviderNumber = pro.Number
	join AdjType adt
		on a.AdjustmentTypeCode = adt.Code
	where a.TransType = 'S'
	group by s.ProviderNumber
		,pro.OrganizationName
		,p.Number
		,p.AliasName
		,p.MedicalRecordNo
)A

select *
from #tmpCases tc
order by tc.ProviderNumber asc
	,tc.AdmitDate asc
	,tc.ClientAccountNumber asc

select tc.ProviderNumber
	,tc.OrganizationName
	,count(distinct tc.PatientNumber) as Cases
	,sum(tc.CharityAmount) as CharityAmount
from #tmpCases tc
group by tc.ProviderNumber
	,tc.OrganizationName
order by tc.ProviderNumber asc

drop table #tmpCases