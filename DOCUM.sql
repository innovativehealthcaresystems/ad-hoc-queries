--select top 10 PatientNumber, fromdate, *
--from service s
--where ServiceItemCode = 'docum'
--order by s.fromdate desc

if exists (select User1 from SIVSystem.dbo.Company where DatabaseName = db_name() and User1 not like 'E%')
begin 
	select sum(A.ServiceCount) as ServiceCount
		,sum(A.Pseudo) as Pseudo
		,sum(A.ServiceItem) as ServiceItem
		,sum(A.Fee) as Fee
		,max(A.User1) as User1
	from (
		select count(*) as ServiceCount 
		,0 as Pseudo
		,0 as ServiceItem
		,0 as Fee
		,'' as User1
		from service s where s.ServiceItemCode = 'docum'

		union

		select 0 as ServiceCount
			,count(*) as Pseudos
			,0 as ServiceItem
			,0 as Fee
			,'' as User1
		from IHSI_PseudoCodes pc
		where pc.Code = 'Docum'

		union

		select 0 as ServiceCount
			,0 as Pseudos
			,count(*) as ServiceItem
			,0 as Fee
			,'' as User1
		from ServiceItem si
		where si.Code = 'docum'

		union

		select 0 as ServiceCount
			,0 as Pseudos
			,0 as ServiceItem
			,count(*) as Fee
			,'' as User1
		from Fee f
		where f.ServiceItemCode = 'docum'
		
		union

		select 0 as ServiceCount
			,0 as Pseudos
			,0 as ServiceItem
			,0 as Fee
			,User1 
		from SIVSystem.dbo.Company 
		where DatabaseName = db_name() and User1 not like 'E%'
	)A
end