select
	st.Staging_ID
	,st.StagedDate as PatientStageDate
	,st.VerifiedDate
	,i.StagedDate as ImageStagedDate
	,st.TI_DateOfService
	,DATEDIFF(day, st.TI_DateOfService, st.VerifiedDate) as DaysBetweenDOSAndVer
	,DATEDIFF(day, st.TI_DateOfService, i.StagedDate) as DaysBetweenDOSAndEligibill
	,DATEDIFF(day, st.StagedDate, i.StagedDate) as DaysBetweenPatientStagingAndEligibillLinking
	,case
		when i.StagedDate < st.VerifiedDate
			then 'Before Verification'
		else 'After Verification'
	end as ImageRecieved
from IHSI_Staging st
join IHSI_ImageFiles i
	on st.Staging_ID = i.Staging_ID
	and st.VerifiedDate >= dateadd(week,-1,getdate())
	and i.ImageFileName like '%ELGB%'
order by st.Staging_ID asc
