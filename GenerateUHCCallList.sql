/*Generate call list*/
use LA_STPH_STEPG

begin
select		
	pp.PatientNumber
	,p.Name as PatientName
	,cast(p.Birthdate as date) as PatientBirthdate
	,cast(s.fromdate as date) as DOS
	,pp.SubscriberName as GuarantorName
	,cast(pp.SubscriberBirthdate as date) as GuarantorBirthdate
	,pp.SubscriberPhoneNo	as GuarantorPhoneNo
	,cast(GETDATE() as date) as DateGenerated
from service s
left join PatientPayor pp
	on s.PatientNumber = pp.PatientNumber
		and pp.Type = 'G'
left join patient p
	on s.PatientNumber = p.Number
join ihsi_uhcpatientscalled pc
	on s.PatientNumber = pc.patientnumber
	and pc.called = 'N'
group by pp.PatientNumber
	,p.Name
	,p.Birthdate
	,cast(s.fromdate as date)
	,pp.SubscriberName
	,pp.SubscriberBirthdate
	,pp.SubscriberPhoneNo
end

/*Mark patients as called*/
begin
update pc
set called = 'Y'
from service s
left join PatientPayor pp
	on s.PatientNumber = pp.PatientNumber
		and pp.Type = 'G'
left join patient p
	on s.PatientNumber = p.Number
join ihsi_uhcpatientscalled pc
	on s.PatientNumber = pc.patientnumber
	and pc.called = 'N'
end

