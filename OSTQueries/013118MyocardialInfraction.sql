use LA_OLOA_WPEPG

create table #tmpICD (
	ICDTen nvarchar(max)
)

insert into #tmpICD values
('I21.A9')
,('I21.4')
,('I21.9')
,('I21.11')
,('I21.19')
,('I21.3')
,('I21.29')
,('I21.21')
,('I21.00')
,('I21.09')
,('I21.02')

select distinct s.PatientNumber as Acct
	,p.MedicalRecordNo as MRN
	,p.AliasName as HID
	,cast(s.FromDate as date) as DOS
	,case
		when s.ICDTen1 in (select * from #tmpICD) then ICDTen1
		when s.ICDTen2 in (select * from #tmpICD) then ICDTen2
		when s.ICDTen3 in (select * from #tmpICD) then ICDTen3
		when s.ICDTen4 in (select * from #tmpICD) then ICDTen4
	end as ICD10
from service s
left join adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '019'
left join patient p
	on s.PatientNumber = p.Number
where isnull(s.TransToSvcNum, '') = ''
	and a.ServiceNumber is null
	and s.FromDate >= '2018-01-01'
	and (s.ICDTen1 in (select * from #tmpICD)
		or s.ICDTen2 in (select * from #tmpICD)
		or s.ICDTen3 in (select * from #tmpICD)
		or s.ICDTen4 in (select * from #tmpICD))
order by DOS asc
	,HID asc

drop table #tmpICD
