delete i1
from ihsi_imagefiles i1
join IHSI_Staging st
	on i1.Staging_ID = st.Staging_ID
join IHSI_Eligibil_Import ei1
	on st.PatientNumber = right(ei1.TraceNumber,9)
	and left(ei1.CoverageType,2) <> 'in'
left join IHSI_Eligibil_Import ei2
	on st.PatientNumber = right(ei2.TraceNumber,9)
	and left(ei2.CoverageType,8) = 'inactive'
where i1.ImageFileName like '%elgb%'
	and ei2.TraceNumber is not null
