create table #tmpICD (
	ICDTen nvarchar(max)
)

insert into #tmpICD values
	('K02.7')
	,('K02.9')
	,('K03.81')
	,('K04.6')
	,('K04.7')
	,('K05.00')
	,('K05.01')
	,('K05.10')
	,('K05.11')
	,('K08.89')
	,('K08.9')
	,('K00.7')
	,('K01.1')

--select *
--from #tmpICD

select distinct pt.AliasName as ClientAcctNumber
	,s.PatientNumber
	,cast(s.FromDate as date) as DOS
	,s.ProviderNumber
	,p.OrganizationName
from Service s
left join Provider p
	on s.ProviderNumber = p.Number
left join patient pt
	on s.PatientNumber = pt.Number
where s.FromDate >= '2019-01-01'
	and (s.ICDTen1 in (select * from #tmpICD)
		or s.ICDTen2 in (select * from #tmpICD)
		or s.ICDTen3 in (select * from #tmpICD)
		or s.ICDTen4 in (select * from #tmpICD))
order by s.PatientNumber asc

drop table #tmpICD