select
	s.PatientNumber

	--,s.EnteredByID
	--,s.EnteredDate
	--,s.LastUpdatedBy
	--,s.LastUpdatedDate
	,s.FromDate
	,s.Comment
	,s.Number as ServiceNumber
	,s.ServiceItemCode
	,s.ServiceStatusCode
	--,s.Amount * s.units as Charges
	,case	
		when a.Number is not null then 'Y'
		else 'N'
	end as HasAdmissionNumber
	,s2.Number as OtherServiceNumber
	,s2.ServiceItemCode as OtherServiceItemCode
	,case
		when adj.Posted is null then 'N'
		else 'Y'
	end as OtherServiceInvalidated
	,sum(a3.Amount) as AmountInCollections
from service s
left join admission a
	on s.AdmissionNumber = a.number
left join service s2
	on s.PatientNumber = s2.PatientNumber
	and s.Number <> s2.Number
	and s2.TransToSvcNum = ''
	and s2.ServiceItemCode in ('99285'
								,'99284'
								,'99283'
								,'99282'
								,'99281'
								,'99291')
left join adjustment adj
	on s2.Number = adj.ServiceNumber
	and adj.AdjustmentTypeCode = '019'
left join Adjustment a2
	on s.Number = a2.ServiceNumber
	and a2.AdjustmentTypeCode = '019'
left join Adjustment a3
	on s2.Number = a3.ServiceNumber
	and a3.AdjustmentTypeCode in ('045','080')
	--and a3.TransType = 'S'
where s.comment like '%corrected%'
	and s.PostedPeriod = '201809'
	and s.TransToSvcNum = ''
	and a2.ServiceNumber is null
	and (s.LastUpdatedDate >= '2018-10-02' or adj.EnteredDate >= '2018-10-02') ---Added to check status of corrections
group by
	s.PatientNumber
	,s.Number
	--,s.EnteredByID
	--,s.EnteredDate
	,s.FromDate
	,s.ServiceItemCode
	,s.ServiceStatusCode
	,s.Amount
	,s.Units
	,a.Number
	,s2.ServiceItemCode
	,adj.Posted
	,s.Comment
	,s2.Number
	--,s.LastUpdatedBy
	--,s.LastUpdatedDate
--order by s.EnteredDate desc
order by s.PatientNumber asc
