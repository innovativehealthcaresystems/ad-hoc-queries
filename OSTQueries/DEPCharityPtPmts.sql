select *
into #tmpPatients
from
(
	select c.AcctNo
		,cast(left(c.AdmitDate, 10) as date) as AdmitDate
		,p.Number as PatientNumber
		,cast(s.FromDate as date) as FromDate
		,case
			when sum(a.amount) is null then 'N'
			else 'Y'
		end as Collections
		,max(pyr.TypeCode) as MaxTypeCode
			---- as AmountInCollections
	from IHSI_DEPCharity c
	join patient p
		on c.AcctNo = p.AliasName
	join service s
		on p.Number = s.PatientNumber
		and s.Amount > 0.01
	left join adjustment a	--Accounts that have been in collections need to go to Sharon/Sandra to look at and manually	handle.
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode in ('080','045')
	left join Payment pmt
		on s.Number = pmt.ServiceNumber
	left join CashReceipt cr
		on pmt.CashReceiptNumber = cr.Number
	left join Payor pyr
		on cr.PayorNumber = pyr.Number
		and pyr.TypeCode = 'A'
	----left join IHSI_MGTPayments pay	--Fails to classify pt payments correctly
	----	on s.Number = pay.svcnumber
	----left join payor pyr
	----	on pay.pmtpayor = pyr.Number
	----	and pyr.TypeCode = 'A'
	where c.OriginalHospitalService = 'EEM'
		and Abs(datediff(day, cast(left(c.AdmitDate, 10) as date), cast(s.FromDate as date))) < 7 --Make sure their DOS is	somewhat close to ours
		--and c.Imported = 'N'	----Removed 1/18/19 by Justin for testing
	group by c.AcctNo
		,cast(left(c.AdmitDate, 10) as date)
		,p.Number
		,cast(s.FromDate as date)
		--,pyr.TypeCode
) as A

/*Pull services that have a positive balance and haven't turned to collections and adjust them automatically*/
select *
into #tmpAdjustments
from
(
	select tp.AcctNo
		,tp.PatientNumber
		,s.Number as ServiceNumber
		,s.ServiceStatusCode
		,s.LastUpdatedBy
		,s.LastUpdatedDate
		,s.FromDate
		,s.Amount * s.Units as ServiceCharge
		,bbs.Balance as ServiceBalance
	from #tmpPatients tp
	join service s
		on tp.PatientNumber = s.PatientNumber
		and s.Amount > 0
	join IHSI_BalanceByService bbs
		on s.Number = bbs.ServiceNumber
	where tp.Collections = 'N'
		--and bbs.Balance > 0
		and isnull(tp.MaxTypeCode, 'Z') = 'A'
	group by tp.AcctNo
		,tp.PatientNumber
		,s.Number
		,tp.Collections
		,s.ServiceStatusCode
		,s.LastUpdatedBy
		,s.LastUpdatedDate
		,s.FromDate
		,bbs.Balance 
		,s.Amount
		,s.Units
) as B

select *
from #tmpAdjustments ta

--select *
--from #tmpPatients tp
--where tp.MaxTypeCode = 'A'

drop table #tmpPatients
drop table #tmpAdjustments