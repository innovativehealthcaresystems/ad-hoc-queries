/*
--------------------------------------------------------------------------
-- When executed will perform the following:
--   *Create an account in userrec table within SivSystem
--   *Create a listing in staff table for each database listed in SivSystem

-- Last updated on 6/19/2015 by Cheryl - Removed code that added the new user to the template db on Daphane, since I deleted that db. 
	We'll be using the new template db on Melia going forward.  NOTE: I have not updated the code below to add the new user by default 
	to Melia.X_TemplateCortexDB, so BE SURE TO INCLUDE MELIA.X_TemplateCortexDB WHEN RUNNING VIA SQL MULTI-SCRIPT.

-- Other Mods: 
		- 15-JUL-2013 by Jeff Price - converted script to run via SQL Multi-Script AND now adds records to template and SIVSYSTEM 
			databases w/o specifically choosing these DB's in SQL Multi-Script.

NOTE:  You might want to make sure the SQL Multi-Script settings are setup to run in sequence, not parallel, 
		otherwise you might see errors like this, which I think was caused by two databases trying to run at the same time.

	"Msg 2601, Level 14, State 1, Line 97
	Cannot insert duplicate key row in object 'dbo.Userrec' with unique index 'UserRec0'. The duplicate key value is (U, TNULL).
	Cannot insert duplicate key row in object 'dbo.UserGrp' with unique index 'UserGrp0'. The duplicate key value is (TNULL, MB).
	The statement has been terminated.
	The statement has been terminated."


-- 4/23/2015: Use the new default password of TemporaryPassword1 (commented below in the INSERT to the UserRec table) after the upgrade to Cortex v10 
---------------------------------------------------------------------------
*/

DECLARE @lc_Initials VarChar(3), @lc_Name VarChar(30), @lc_Number Char(4), @lc_SOLID varchar(47), @lc_WindowsID char(85)
	, @lc_PrimaryGroup char(47), @lc_SecondaryGroup Char(47), @lc_TertiaryGroup char(47), @lc_DeptCode char(3)

SET @lc_Name = 'Sunkari Sravanthi' -- User first and last name
SET @lc_Number = '1080' -- CRITICAL!! - User number must be four digits in length.  **For AGS users, user the next AG # as seen in Staff table, for Temp users use Txxx
SET @lc_SOLID = 'ssravanthi' -- Username will be made uppercase in the insert below.
SET @lc_WindowsID = 'ssravanthi' -- Windows Username will be made uppercase in the insert. Enter without the IHSI\. 7/2/15: To be used for validation in ICIS on the Cortex password reset screen.
SET @lc_SecondaryGroup = '' -- User Secondary group (This is usually REIMB or AR, but could also be PSR, CLINICAL, COLLECTION, GRM, or DATA ENTRY)
SET @lc_TertiaryGroup = '' -- User tertiary group (Rarely used)
SET @lc_DeptCode = 'I06' --This is a three-digit code, but the default is 1. The values are below, but this should be provided by the supervisor upon hiring a new employee.

----Find Next Available coder #
--exec cain.IHS_DB.dbo.IHSI_NextAvailCoderNum

--select LTRIM(RTRIM(UserName)) as UserName, LTRIM(RTRIM(UserId)) as UserID, WindowsUserAcct 
--from SIVSystem.dbo.UserRec
--where UserID = @lc_SOLID

--Find Next Available AGS, GRCP, or Temp Employee #
select Name, Number, DepartmentCode
from Staff
--where number like 'A%' and Number < 'A999'--AGS users
--where number like 'T%' --Temp agency users
where number like 'M%'	--MiraMed
order by number desc

--select *
--from Staff
--where name like '%suggs%'

--select *
--from SIVSystem.dbo.UserGrp
--where UserId like '%deakle%'

--select LTRIM(RTRIM(UserName)), LTRIM(RTRIM(UserId)), WindowsUserAcct 
--from SIVSystem.dbo.UserRec
--where UserName like '%suggs%'

----Update a user's password to TemporaryPassword1
--update [SIVSystem].[dbo].[Userrec]
--set Password = '4818S+vlbmnY54iYUZ6ISE2SX/RlVKMWdCk6CpyzZZMftjU=004da397-1b26-459a-aee4-226448a8b1e54939a094-6682-40ca-bb20-bd3433b51a9daDUV9JYAi3kvxIz4z6fhdg==' --[Password]
--where WindowsUserAcct like 'IHSI%'
--and UserId <> 'SYSADMIN'


/*
-- AVAILABLE GROUPS --

SELECT GroupId 
FROM USERGRP
GROUP BY GroupId 


ADMINS                                         
AR                                             
CLINICAL                                       
COLLECTION                                     
DATA ENTRY                                     
GRM                                            
LEADS                                          
MB                                             
OPS MGMT                                       
PSR                                            
REIMB                                          
SUPERVISORS                                   
*/

/*
Availalble values for the Dept. Code variable
Code	Name						BillingCode
I06		02 Coding Services			25000 --USE THIS FOR CODERS
I01		Accounting					12000
I02		Administration				13000
I08		Cash Receipts				27000
I03		Client Relations			15000
I13		Clin Bill					29500
I04		Coding Services				16000 
I12		Collections Refunds			29000
I09		Data Entry					27500
I11		Gideon						28500
I15		IT							40000
I05		Onsite Coordinators Dept	25000
I07		Patient Services			26000
I10		Reimbursements				28000
I14		Synovia						30000
*/


------------------------------------------------------------------------
--
--               DO NOT MAKE CHANGES BELOW THIS LINE!                 --
--
------------------------------------------------------------------------
SET @lc_PrimaryGroup = 'MB'  -- User Primary group (should always be MB)

-- set user initials automatically based on name.
SET @lc_Initials = (
	SELECT SUBSTRING(@lc_Name, 1, 1) +     --First initial
    SUBSTRING(@lc_Name, CHARINDEX(' ', @lc_Name) + 1, 1) +    --Middle/Last initial
    CASE WHEN 0 <>  CHARINDEX(' ', @lc_Name, CHARINDEX(' ', @lc_Name) + 1) -- More than two words 
        THEN SUBSTRING(@lc_Name, CHARINDEX(' ', @lc_Name, CHARINDEX(' ', @lc_Name) + 1) + 1, 1)  --Last initial
    ELSE '' --Have to add empty string to avoid NULLing entire result
    END
    )

-- If employee number is not exactly four digits in length, halt the process.
IF LEN(@lc_Number) <> 4
BEGIN
	PRINT 'INCORRECT EMPLOYEE NUMBER LENGTH, HALTING PROCESS!'
	RETURN
END


-- Insert record into current database
IF NOT EXISTS (select Number from dbo.Staff where Number = @lc_Number)
	BEGIN	
		INSERT INTO dbo.Staff (Active, DepartmentCode, Initials, LastUpdatedBy, LastUpdatedDate, Name, Number, Password, SolomonUserID) 
		VALUES ('Y', @lc_DeptCode, @lc_Initials, '9999', GETDATE(), @lc_Name, @lc_Number, '', UPPER(@lc_SOLID))
	END
 
---- 6/19/2015: Removed by Cheryl - I deleted the X_EmptyCortexDatabase db on Daphne, since we'll be using Melia.X_TemplateCortexDB going forward.
---- Insert record into template database.
--IF EXISTS (SELECT name FROM sys.databases where name = 'X_EmptyCortexDatabase')
--	BEGIN
--		IF NOT EXISTS (select Number from X_EmptyCortexDatabase.dbo.Staff where Number = @lc_Number)
--			BEGIN
--				INSERT INTO DAPHNE.X_EmptyCortexDatabase.dbo.Staff (Active, DepartmentCode, Initials, LastUpdatedBy, LastUpdatedDate, Name, Number, Password, SolomonUserID) 
--				VALUES ('Y', @lc_DeptCode, @lc_Initials, '9999', GETDATE(), @lc_Name, @lc_Number, '', @lc_SOLID)
--			END
--	END




-- Add record to SIVSYSTEM database but first remove any existing UserRec and UserGrp entries.
DELETE FROM [SIVSystem].[dbo].UserGrp WHERE UserID = @lc_SOLID
DELETE FROM [SIVSystem].[dbo].[UserRec] WHERE UserID = @lc_SOLID
	

--Creates new userid in UserRec table if one does not already exist
IF NOT EXISTS (select UserID from [SIVSystem].[dbo].[Userrec] where UserID = @lc_SOLID)
	BEGIN
		INSERT INTO [SIVSystem].[dbo].[Userrec]
			([AlignFormGrid]
		      ,[AppServerUser]  --3/26/2013:  Possibly needed for new Cortex versions. I tried to add a user to Hector and it said that you can't enter nulls into this field.
			  ,[AutoDecimal]
			  ,[AutoTab]
			  ,[BrowserOnTop]
			  ,[CacheFavorite]
			  ,[CacheMenu]
			  ,[CodeOnTop]
			  ,[CodeFont]
			  ,[CodeFontSize]
			  ,[CodeTabWidth]
			  ,[ColorPref]
			  ,[ColorPrefPV]
			  ,[CustomGroup]
			  ,[DefaultUser]  -- needed for new Cortex version
			  ,[EMailAddress]
			  ,[EnterForTab]
			  ,[FormGridHeigth]
			  ,[FormGridWidth]
			  ,[HomePage]
			  ,[IntLogon]
			  ,[LastDatabase]
			  ,[Location]
			  ,[MenuGroup]
			  ,[OutputFileName]
			  ,[OutputPath]
			  ,[OutputRouting]
			  ,[Password]
			  ,[Phone]
			  ,[PropertiesOnTop]
			  ,[RecType]
			  ,[S4Future01]
			  ,[S4Future02]
			  ,[S4Future03]
			  ,[S4Future04]
			  ,[S4Future05]
			  ,[S4Future06]
			  ,[S4Future07]
			  ,[S4Future08]
			  ,[S4Future09]
			  ,[S4Future10]
			  ,[SaveSettingsOnExit]
			  ,[ShowFormGrid]
			  ,[ShowGridLines]
			  ,[ShowGridRowNbrs]
			  ,SQMOptIn  -- needed for new Cortex version
			  ,[User1]
			  ,[User2]
			  ,[User3]
			  ,[User4]
			  ,[UserId]
			  ,[UserName]
			  ,[WindowsUserAcct]
			  )
		SELECT [AlignFormGrid]
			  ,[AppServerUser]  --3/26/2013:  Possibly needed for new Cortex versions. I tried to add a user to Hector and it said that you can't enter nulls into this field.
			  ,[AutoDecimal]
			  ,[AutoTab]
			  ,[BrowserOnTop]
			  ,[CacheFavorite]
			  ,[CacheMenu]
			  ,[CodeOnTop]
			  ,[CodeFont]
			  ,[CodeFontSize]
			  ,[CodeTabWidth]
			  ,[ColorPref]
			  ,[ColorPrefPV]
			  ,[CustomGroup]
			  ,'1' --[DefaultUser]  needed for new Cortex version
			  ,'' as EMailAddress
			  ,[EnterForTab]
			  ,[FormGridHeigth]
			  ,[FormGridWidth]
			  ,[HomePage]
			  ,[IntLogon]
			  ,[LastDatabase]
			  ,[Location]
			  ,'EVERYONE' as MenuGroup
			  ,[OutputFileName]
			  ,[OutputPath]
			  ,[OutputRouting]
			  ----Default password below translates to "TemporaryPassword1". Uncomment the line below after the upgrade to Cortex v10
			  ,'4818S+vlbmnY54iYUZ6ISE2SX/RlVKMWdCk6CpyzZZMftjU=004da397-1b26-459a-aee4-226448a8b1e54939a094-6682-40ca-bb20-bd3433b51a9daDUV9JYAi3kvxIz4z6fhdg==' --[Password]
			  ,[Phone]
			  ,[PropertiesOnTop]
			  ,[RecType]
			  ,[S4Future01]
			  ,[S4Future02]
			  ,[S4Future03]
			  ,[S4Future04]
			  ,[S4Future05]
			  ,[S4Future06]
			  ,[S4Future07]
			  ,[S4Future08]
			  ,[S4Future09]
			  ,[S4Future10]
			  ,[SaveSettingsOnExit]
			  ,[ShowFormGrid]
			  ,[ShowGridLines]
			  ,[ShowGridRowNbrs]
			  ,'' --SQMOptIn  needed for new Cortex version
			  ,[User1]
			  ,[User2]
			  ,[User3]
			  ,[User4]
			  ,UPPER(@lc_SOLID) as UserId
			  ,UPPER(@lc_Name) as UserName
			  ,'IHSI\' + UPPER(@lc_WindowsID) --[WindowsUserAcct]
		  FROM [SIVSystem].[dbo].[Userrec]
		  where userid = 'JPRICE'
	END


--Checks that at least one group is specified
IF @lc_primaryGroup = ''
	Print 'Please specify a user group'
ELSE
	BEGIN
		INSERT INTO [SIVSystem].[dbo].UserGrp (GroupID,user1,user2,User3,User4,UserId)
		Values (@lc_PrimaryGroup,'','','','',UPPER(@lc_SOLID))
	END
--If a secondary group is specified, add it.
IF @lc_secondaryGroup <> ''
	BEGIN
		INSERT INTO [SIVSystem].[dbo].UserGrp (GroupID,user1,user2,User3,User4,UserId)
		Values (@lc_SecondaryGroup,'','','','',UPPER(@lc_SOLID))
	END
ELSE
--If a tertiary group is specified, add it.
IF @lc_TertiaryGroup <> ''
	BEGIN
		INSERT INTO [SIVSystem].[dbo].UserGrp (GroupID,user1,user2,User3,User4,UserId)
		Values (@lc_TertiaryGroup,'','','','',UPPER(@lc_SOLID))
	END



/*
---If you spell someone's name wrong, here are the queries you need to run to fix it.

select *
from Staff 
where number = '1065'


select *
from [SIVSystem].[dbo].[Userrec] 
where userid = 'PMARAN'

select *
from [SIVSystem].[dbo].UserGrp
where userid = 'PMARAN'


--update Staff 
--set name = 'Pennarasi Maran'
--	, solomonuserid = 'PMARAN'
--where number = '1065'

--update [SIVSystem].[dbo].[Userrec] 
--set userid = 'PMARAN'
--	, username = 'Pennarasi Maran'
--  , windowsuseracct = 'IHSI\PMARAN'
--where userid = 'PHARAN'
 
--update [SIVSystem].[dbo].UserGrp
--set userid = 'PMARAN'
--where userid = 'PHARAN'

--------------------------------------------------------------------

If a user's account already exists in Cortex, but their employee ID has changed, update the following fields
on their old account, so that you can create a new account with the new employee ID.

----Update the Staff tables and rename their old user ID to _old.
--update Staff
--set SolomonUserId = 'SSUGGS_old'
--where name like '%suggs%'


----Update the UserRec tables and rename their old user ID's to _old.
--update SIVSystem.dbo.UserRec
--set UserId = 'SSUGGS_old', WindowsUserAcct = 'IHSI\SSUGGS_old'
--where UserName like '%suggs%'
*/




------------------------------------------------------------------------
/*
1/12/2016

Used for SR 16965 from Shelby, but ultimately it was to fix SR 16925 where we updated someone's temporary/contract user ID to an IHSI employee ID.

ACTIONS: Updated the User ID linked to old transactions in ICIS and Cortex for a REIMB user to their new User ID, 
	b/c ICIS QuickView failed to open for patients that had notes created under their old user ID.  
	This is b/c ICIS requires the name associated with a patient's note to not be NULL.

MISC:
If you should ever have a need to do this in the future, then you'll probably need to expand on the 
tables below, i.e. IHSI_Staging, IHSI_CodingEntries, other IHSI coding tables, IHSI Refunds tables, IHSI PSR tables, etc.
This was created for a REIMB user, so those tables weren't necessary.

*****THE NEXT TIME SOMEONE WANTS YOU TO UPDATE THEIR USER ID TO AN IHSI EMPLOYEE ID, TELL THEM TO "FORGET ABOUT IT"....OR
JUST UPDATE THEIR OLD ACCOUNT'S NAME TO "_OLD" AND CREATE A BRAND NEW ACCOUNT FOR THEM IN ICIS AND CORTEX.  
SEE THE QUERY ABOVE THAT WAS USED FOR THE USER, SSUGGS.
*/

/*
select * from PatientNote where EnteredBy = 'RH04'
select * from Patient where LastUpdatedBy = 'RH04'
select * from Service where EnteredByID = 'RH04'
select * from Service where LastUpdatedBy = 'RH04'
select * from Adjustment where EnteredByID = 'RH04'
select * from PatStmtRun where StaffID = 'RH04'
select * from IHSI_ReimbActivity where WorkedByID = 'RH04'
select * from IHSI_ReimbActivity where EscalatedToID = 'RH04'
select * from IHSI_ReimbQueue where AssignedToID = 'RH04'

update PatientNote
set EnteredBy = '0528'
where EnteredBy = 'RH04'

update Patient
set LastUpdatedBy = '0528'
where LastUpdatedBy = 'RH04'

update Service
set EnteredByID = '0528'
where EnteredByID = 'RH04'

update Service
set LastUpdatedBy = '0528'
where LastUpdatedBy = 'RH04'

update Adjustment
set EnteredByID = '0528'
where EnteredByID = 'RH04'

update PatStmtRun
set StaffID = '0528'
where StaffID = 'RH04'

update IHSI_ReimbActivity
set WorkedByID = '0528'
where WorkedByID = 'RH04'

update IHSI_ReimbActivity
set EscalatedToID = '0528'
where EscalatedToID = 'RH04'

update IHSI_ReimbQueue
set AssignedToID = '0528'
where AssignedToID = 'RH04'

*/

---------------------------------------------------------------------------------
