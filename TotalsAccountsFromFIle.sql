select
	distinct ie.Acct_No
from DEPInsuranceExamples ie
join patient p
	on ie.Acct_No = p.AliasName
	and ie.Ins_Plan_1_Policy__ is not null			--Check for real policy numbers
	and ie.Ins_Plan_1_Policy__ not in ('*01','01')
	and not exists (
		select aliasname
		from IHSI_DemographicImportTemplateUpdatable
		where ie.Acct_No = AliasName
			and ie.Ins_Plan_1_Policy__ = Ins1PolicyNumber
		)