select count(distinct x.PatientNumber) as PatientUpdatesReceived
	,cast(datepart(year,x.ServiceDate) as char(4)) + cast(DATEPART(month,x.servicedate) as char(2)) as ServiceMonth
from (
		select s.PatientNumber
			,dem.Ins1Name
			,dem.Ins1CompanyID
			,dem.ins1address1
			,dem.Ins1PolicyNumber
			,dem.Ins1GroupNumber
			,dem.Ins2Name
			,dem.Ins2CompanyID
			,dem.Ins2Address1
			,dem.Ins2PolicyNumber
			,dem.Ins2GroupNumber
			,dem.ServiceDate
			,max(dem.HL7MessageTime) as MessageTime
			,st.VerifiedDate
			,datediff(day,st.verifieddate,max(dem.hl7messagetime)) as DaysBetweenVerAndUpdate
			,datediff(day,dem.ServiceDate,st.VerifiedDate) as DaysBetweenDOSAndVer
			,ei.MemberID
			,ei.SentPayerName
			,ei.RecdPayerName
			,ei.CoverageType
			,ei.GroupNumber
			,ei.BenefitDescriptions
			,ei.TPLPayerName
			,ei.TPLSubscriberId
			,ei.TPLGroupNumber
			,ei.TPLPolicyNumber
			,ei.CreatedDateTime
			--,st.VerifiedDate
			,ei.TPLEntityType
		from service s
			join patient p
				on s.PatientNumber = p.Number
			left join IHSI_DemographicImportTemplateUpdatable dem
				on p.AliasName = dem.AliasName
			left join IHSI_Eligibil_Import ei
				on s.PatientNumber = right(ei.TraceNumber,9)
			join IHSI_Staging st
				on s.PatientNumber = st.PatientNumber
		where s.FromDate >= '1993-03-22'
			and s.PayorNumber = '0000001'
			and coalesce (
				dem.Ins1Name
				,dem.Ins1CompanyID
				,dem.ins1address1
				,dem.Ins1PolicyNumber
				,dem.Ins1GroupNumber
				,dem.Ins2Name
				,dem.Ins2CompanyID
				,dem.Ins2Address1
				,dem.Ins2PolicyNumber
				,dem.Ins2GroupNumber
				,ei.MemberID
				,ei.SentPayerName
				,ei.RecdPayerName
				,ei.CoverageType
				,ei.GroupNumber
				,ei.BenefitDescriptions
				,ei.TPLPayerName
				,ei.TPLSubscriberId
				,ei.TPLGroupNumber
				,ei.TPLPolicyNumber
				,ei.TPLEntityType) is not null
			and ((isnull(dem.Ins1PolicyNumber, '') <> ''
					and Ins1PolicyNumber <> '*01')
				or isnull(dem.Ins2PolicyNumber, '') <> ''
				or (isnull(ei.MemberID, '') <> ''
					and ei.CoverageType not like '%inactive%')
				or (isnull(ei.TPLSubscriberId, '') <> ''
					and ei.CoverageType not like '%inactive%')
				or (isnull(ei.TPLPolicyNumber, '') <> ''
					and ei.CoverageType not like '%inactive%'))
			and dem.ins1name <> 'SELF PAY'
		group by s.PatientNumber
			,dem.Ins1Name
			,dem.Ins1CompanyID
			,dem.ins1address1
			,dem.Ins1PolicyNumber
			,dem.Ins1GroupNumber
			,dem.Ins2Name
			,dem.Ins2CompanyID
			,dem.Ins2Address1
			,dem.Ins2PolicyNumber
			,dem.Ins2GroupNumber
			,ei.MemberID
			,ei.SentPayerName
			,ei.RecdPayerName
			,ei.CoverageType
			,ei.GroupNumber
			,ei.BenefitDescriptions
			,ei.TPLPayerName
			,ei.TPLSubscriberId
			,ei.TPLGroupNumber
			,ei.TPLPolicyNumber
			,ei.CreatedDateTime
			,ei.TPLEntityType
			,st.VerifiedDate
			,dem.ServiceDate
		--order by s.PatientNumber asc
		--	,max(dem.HL7MessageTime) asc
		--	,ei.CreatedDateTime asc
)x
where DaysBetweenVerAndUpdate > 0
group by cast(datepart(year,x.ServiceDate) as char(4)) + cast(DATEPART(month,x.servicedate) as char(2))

order by servicemonth desc