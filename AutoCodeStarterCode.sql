/*
1. This will apply on a db by db basis. For now let's only apply it to LA OLOL PEPA
*/

--2. Pull records out of the coding queue that have images that contain pseudocodes in the image file name. Starter code taken from procedure IHSI_CodingQueueByHoursQueue.

select cq.Staging_ID
	,cq.PatientNumber
	,i.ImageFileName
from
(
-----------------------Below was taken from the coding queue procedure-----------
	select
		st.Staging_ID,
		Case
			WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
			WHEN MAX(i.StagedDate)<=st.stagedDate THEN MAX(st.StagedDate) --Modified by Justin T. on 5/31/2017 - changed output from MAX(I.STAGEDDATE) to MAX(ST.STAGEDDATE)
		END AS MostRecentDate,
		st.ti_dateofservice,
		st.TI_AccountNumber,
		ISNULL(p1.typecode, 'XX') AS PayorType, st.PatientNumber,
		'NA' as SkipReason, 'NA' as StatusCheck, 0 AS CodeCount , pp1.PayorNumber
	from IHSI_Staging as st
		LEFT JOIN IHSI_ImageFiles as i on st.Staging_ID=i.Staging_ID
		LEFT JOIN PatientPayor as PP1 on pp1.patientnumber = st.patientnumber AND pp1.type = 'P'
		LEFT JOIN Payor as P1 on P1.number = pp1.payornumber 
		LEFT JOIN IHSI_PayorTypeQueueOrder as PT on ISNULL(p1.typecode, 'XX') = PT.PayorType AND PT.Screen = 'Coding'
		LEFT JOIN IHSI_SkipLog as sl with(nolock) on st.Staging_ID=sl.Staging_ID
			and sl.ReviewComplete = 'Y' AND sl.Active = 'Y'
		LEFT JOIN IHSI_SkipReasons sr with(nolock) on sl.SkipReasonCode = sr.SkipCode
	where (st.Skipped in ('N') or (st.Skipped in ('Y') and sr.skiptype IN ('C', 'D', 'X', 'Y')))
		and st.Coded='N'
		and st.CodingLock = 'N'			--- Added 5/31/17 by Justin T. as found in procedure IHSI_CodingQueue
		--and ST.ProviderNumber IN (SELECT ProviderNumber FROM @ProviderGroupNumberTable)
		--and ((DATEADD(Hour, @CodingBufferHourDelay, st.StagedDate) < GETDATE()) OR (@CodingBufferDelayAllPayors <> 1 AND pp1.PayorNumber <> '0000001') OR st.CodingTier < 3)
	Group by st.StagedDate,st.Staging_ID,st.TI_AccountNumber, st.PatientNumber,p1.typecode, pp1.PayorNumber,st.ti_dateofservice
	having COUNT(i.image_id)>=1
	--order by MostRecentDate asc--, CASE WHEN @ProviderGroupNumber <> 0 THEN NEWID() 
-----------------------Above was taken from the coding queue procedure-----------
) as CQ
join IHSI_ImageFiles i
	on CQ.Staging_ID = i.Staging_ID
join IHSI_PseudoCodes pc
	on i.ImageFileName like '%' + pc.code + '%'	--This could be used to pluck the pseudocode out of the filename
	and pc.Active = 'Y'
	and i.ImageFileName like '%followup%' --A double check
group by CQ.Staging_ID
	,cq.PatientNumber
	,i.ImageFileName

--3. Pluck out the pseudocode using the above
--4. Mimic the process of coding a chart as demonstrated in video.