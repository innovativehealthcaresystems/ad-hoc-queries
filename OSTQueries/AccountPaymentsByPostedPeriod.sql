select *
into #tmpPayments
from (
	SELECT s.patientnumber,
		s.ProviderNumber,
		pro.OrganizationName, 
		s.serviceitemcode as sicode, 
		s.fromdate, 
		SUM(ISNULL(p.PayAmount, 0.00) + ISNULL(p.AdjAmount, 0.00)) AS PmtAmount
		,p.PostedPeriod
	FROM dbo.IHSI_MGTServices s WITH(NOLOCK) 
		LEFT JOIN dbo.IHSI_MGTPayments p ON s.number = p.SvcNumber
		left join Provider pro
			on s.ProviderNumber = pro.Number
	WHERE s.FromDate >= '2017-01-01'				--Starting DOS here (inclusive)
		and s.FromDate < '2019-01-01'			--Ending DOS here (non-inclusive)
		and p.PostedPeriod >= '201810'			--Starting payment posted period here (inclusive)
		and p.PostedPeriod <= '201901'			--Ending payment posted period here (inclusive)
	GROUP BY s.patientnumber, 
		s.serviceitemcode, 
		s.fromdate
		,p.PostedPeriod
		,s.ProviderNumber
		,pro.OrganizationName
) TP

select *
into #tmpPatients
from (
	select distinct tp.PatientNumber
	from #tmpPayments tp
)TPT

select *
into #tmpLOS
from (
	select distinct tpt.PatientNumber
		,max(s.ServiceItemCode) as LOSCode
	from #tmpPatients tpt
	left join service s
		on tpt.PatientNumber = s.PatientNumber
		and s.ServiceItemCode in ('99281', '99282', '99283', '99284', '99285', '99291')
		and s.TransToSvcNum = ''
	left join Adjustment a
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = '019'
		and a.Amount < 0
	left join adjustment a2
		on s.Number = a2.ServiceNumber
		and a2.AdjustmentTypeCode = '019'
		and a2.Amount > 0
	where a.ServiceNumber is null
		or (a.ServiceNumber is not null and a2.ServiceNumber is not null)	--Think about 019s with 0 amount or reversed 019s (sum = 0)
	group by tpt.PatientNumber
)TLOS

select *
into #tmpAggPayments
from (
	select tlos.PatientNumber
		,p.MedicalRecordNo
		,p.AliasName as AccountNumber
		,cast(tp.FromDate as date) as DOS
		,tp.ProviderNumber
		,tp.OrganizationName as Location
		,isnull(tlos.LOSCode, max(tp.sicode)) as LOSCode
		,tp.PostedPeriod as PaymentPostedPeriod
		,round(sum(tp.PmtAmount), 2) as PaymentsPosted
	from #tmpLOS tlos
	join #tmpPayments tp
		on tlos.PatientNumber = tp.PatientNumber
	join patient p
		on tlos.PatientNumber = p.Number
	group by tlos.PatientNumber
		,p.MedicalRecordNo
		,p.AliasName
		,cast(tp.FromDate as date)
		,tlos.LOSCode
		,tp.PostedPeriod
		,tp.ProviderNumber
		,tp.OrganizationName
)TAP

select tap.*
	,DATEPART(MONTH, tap.DOS) as MOS
	,DATEPART(year, tap.DOS) as YOS
from #tmpAggPayments tap
where tap.PaymentsPosted <> 0
order by tap.DOS asc
	,tap.PatientNumber asc
	,tap.PaymentPostedPeriod asc

drop table #tmpAggPayments
drop table #tmpLOS
drop table #tmpPatients
drop table #tmpPayments

