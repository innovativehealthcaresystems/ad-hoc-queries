select *
into #tmpServices
from (
	select s.PatientNumber
		,s.Number as ServiceNumber
		,s.FromDate as DOS
		,s.PayorNumber
		,p.Name as PayorName
		,p.TypeCode
		,pt.Description
		,s.Amount * s.Units as Charge
		,bbs.Balance
		,s.ServiceStatusCode
		,s.Comment
		,case 
			when s.LastUpdatedDate = '1900-01-01 00:00:00.000' then s.EnteredDate
			else ISNULL(s.LastUpdatedDate, s.EnteredDate) end as LastActionDate
		--,dbo.fn_workdays('2019-02-04','2019-02-16')
		,DATEDIFF(day, ISNULL(s.LastUpdatedDate, s.EnteredDate), GETDATE()) as TotalDaysStagnant
	from service s 
	left join IHSI_BalanceByService bbs
		on s.Number = bbs.ServiceNumber
	left join payor p
		on s.PayorNumber = p.Number
	left join IHSI_PayorType pt
		on p.TypeCode = pt.Code
	where s.ServiceStatusCode in (3, 4, 7, 99)
) A

select ts.PatientNumber
	,ts.ServiceNumber
	,cast(ts.DOS as date) as DOS
	,ts.PayorNumber
	,ts.PayorName
	,ts.TypeCode
	,ts.Charge
	,ts.Balance
	,ts.ServiceStatusCode
	,ts.Comment
	,cast(ts.LastActionDate as date) as LastActionDate
	,dbo.fn_WorkDays(ts.LastActionDate, getdate()) as BusinessDaysStagnant
from #tmpServices ts
where ts.TypeCode = 'D' and ts.ServiceStatusCode = 4 and ts.TotalDaysStagnant > 35 --give Medicaid status 4s 35 days since they are printed to file monthly
		or ((ts.TypeCode <> 'D' or (ts.TypeCode = 'D' and ts.ServiceStatusCode <> 4)) --Wait until 11+ business days after entry/update to allow for claim/statement runs
			and dbo.fn_WorkDays(ts.LastActionDate, GETDATE()) > 10)
drop table #tmpServices

