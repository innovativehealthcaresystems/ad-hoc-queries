select * from AdjType
where Description like '%debt%'
	or Code in (
		'080'
	)

--We need the following data on Bad Debt for the upcoming ECP meeting:
--Facility
--Month of Service
--Visits
--Payer Type Description
--Amount
--These numbers should only be reflective of accounts with the bad debt adjustment code. This data should run from July 2017-Jun 2018

select s.ProviderNumber
	,p.OrganizationName as Facility
	,DATEPART(month, s.FromDate) as MonthOfService
	,DATEPART(year, s.FromDate) as YearOfService
	,count(distinct s.PatientNumber) as Visits
	,pay.TypeCode
	,pt.Description as PayorType
	,round(sum(a.Amount), 2) as BadDebtAdjustments
	--,s.PatientNumber
----------------Testing---------------------
--select s.PatientNumber
--	,a.Amount
--	,a.AdjustmentTypeCode
--	,a.ServiceNumber
--	,a.EnteredByID
--	,*
--------------------------------------------
from service s
join Adjustment a
	on s.Number = a.ServiceNumber
join AdjType adty
	on a.AdjustmentTypeCode = adty.Code
	and (adty.Description like '%bad%debt%'
		or adty.Code in (
			'080'
			))
join Provider p
	on s.ProviderNumber = p.Number
join Payor pay
	on s.PayorNumber = pay.Number
join IHSI_PayorType pt
	on pay.TypeCode = pt.Code
where s.FromDate >= '2017-07-01'
	and s.FromDate < '2018-07-01'

	------------Test-----------------
	--and DATEPART(month, s.FromDate) = 4
	--and pt.Description = 'Blue Cross & Blue Shield'
	--and s.ProviderNumber = '0000001'
	---------------------------------
group by s.ProviderNumber
	,p.OrganizationName
	,DATEPART(month, s.FromDate)
	,DATEPART(year, s.FromDate)
	,pay.TypeCode
	,pt.Description
	--,s.PatientNumber