select
	ie.Acct_No
	--,ie.Carrier_Code_1_Name	--Susan's responses contain multiple names
	,ie.Ins_Plan_1_Policy__
	--,ie.Ins_Plan_1_Group__	--Sometimes the demo feed got multiple group numbers
	--,dem.Ins1Name
	,dem.Ins1PolicyNumber
	--,dem.Ins1GroupNumber
	,cast(dem.ServiceDate as date) as ServiceDate
	,cast(st.VerifiedDate as date) as MostRecentVerifiedDate
	,cast(min(dem.HL7MessageTime) as date) as UpdatedMessageDate
	--,cast(min(dem.InsertDate) as date) as UpdatedMessageInsertDate
	,cast(min(pay.entereddate) as date) as First0PayDate
	,DATEDIFF(day,dem.ServiceDate,st.VerifiedDate) as DelayDaysVerification
	,DATEDIFF(day,dem.ServiceDate,min(dem.hl7messagetime)) as DelayDaysHL7Update
	,DATEDIFF(day,st.VerifiedDate, min(dem.HL7MessageTime)) as UpdateDaysPostVerification
	,case when DATEDIFF(day,st.VerifiedDate, min(dem.HL7MessageTime)) < 0 then 'Prior To Verifiecation'
		else 'After Verification'
	end as UpdateReceived
from DEPInsuranceExamples ie
join IHSI_DemographicImportTemplateUpdatable dem
	on ie.Acct_No = dem.AliasName
	and ie.Ins_Plan_1_Policy__ is not null
	and ie.Ins_Plan_1_Policy__ not in ('01','*01')
	and ie.Ins_Plan_1_Policy__ = dem.Ins1PolicyNumber
join IHSI_Staging st
	on ie.Acct_No = st.TI_AccountNumber
left join service s
	on st.PatientNumber = s.Number
left join payment pay
	on s.Number = pay.ServiceNumber
	and pay.Amount = 0
group by
	ie.Acct_No
	--,ie.Carrier_Code_1_Name
	,ie.Ins_Plan_1_Policy__
	,ie.Ins_Plan_1_Group__
	--,dem.Ins1Name
	,dem.Ins1PolicyNumber
	--,dem.Ins1GroupNumber	--Sometimes the demo feed got multiple group numbers
	,dem.ServiceDate
	,st.VerifiedDate
order by 
	DATEDIFF(day,dem.ServiceDate,min(dem.hl7messagetime)) asc