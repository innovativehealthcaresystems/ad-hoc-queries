
IF OBJECT_ID('tempdb..#tmpzeropay') IS NOT NULL
Begin
  DROP TABLE #tmpzeropay
End
  
  
---To get zero pay EOBReasonText with respect to max(Payment.EnteredDate)
select 
ROW_NUMBER() over(partition by s.Number order by pmt.EnteredDate desc) as Rnum,
--s.PatientNumber, 
s.Number, pmt.EOBReasonText as EOBReasonText, pmt.EnteredDate as DateOfZeroPay--,denadj.ReasonText as zeroPayReasonText
into #tmpzeropay
from Service as s
		left join Payment as pmt on pmt.servicenumber = s.number AND pmt.Amount = 0
		left join adjustment as denadj on denadj.paymentnumber = pmt.number
		  and denadj.entereddate > pmt.entereddate
--where s.PatientNumber in ('000173301','000203268') 
group by s.Number
--,denadj.ReasonText
,pmt.EOBReasonText,pmt.EnteredDate--,s.PatientNumber


--select *from  #tmpzeropay where rnum=1 and PatientNumber in ('000173301','000203268') 


select SvcNumber 
	, ProviderNumber
		, ProviderName
		, PatientNumber
		, HospitalAccountNumber
		, DateOfService
		, ServiceItemCode
		, ICD1Code
		, ICD1Description
		, ICD2Code
		, ISNULL(ICD2Description,'') as ICD2Description
		, Charges
		, y.ServiceAdjustment
		, z.PaymentAdjustment
		, zz.Payments		
		, (Charges + y.ServiceAdjustment) -  (zz.Payments + z.PaymentAdjustment) as Balance
		, BillingClinNumber
		, BillingClinName
		, PayorName
		, PayorNumber 
		, PayorTypeCode
		, PayorTypeCodeDescription
		, CASE WHEN za.DateOfZeroPay IS null THEN 'N' ELSE 'Y' END as ZeroPayYN
		, za.EOBReasonText as DenialReasonCodes
		, ZA.DateOfZeroPay
		, LastClaimDate
		, ServiceStatusCode
		, LastNonPADNoteDate
		, PayorChangeMade
		--,Y.ServiceAdjReasonText
		--,Z.PaymentAdjReasonText
		--,ZA.zeroPayReasonText
from (
	select s.Number as SvcNumber 
		, s.ProviderNumber
		, pro.OrganizationName as ProviderName
		, p.Number as PatientNumber
		, p.AliasName as HospitalAccountNumber
		, s.FromDate as DateOfService
		, s.ServiceItemCode
		, s.ICDTen1 as ICD1Code
		, i1.Description as ICD1Description
		, s.ICDTen2 as ICD2Code
		, i2.Description as ICD2Description
		, sum(s.Amount * s.Units) as Charges
		--, SUM(isnull(pmt.Amount,0)) as Payments
		--, SUM(isnull(pmtAdj.Amount,0)) as PmtAdjustments
		--, SUM(isnull(svcAdj.Amount,0)) as SvcAdjustments
		--, SUM(s.Amount * s.Units) -
		--	 (SUM(isnull(pmt.Amount,0)) + SUM(isnull(pmtAdj.Amount,0)))
		-- + SUM(isnull(svcAdj.Amount,0))
		 --as Balance
		, s.performedbyID as BillingClinNumber
		, cl.name as BillingClinName
		, pyr.Name as PayorName
		, s.PayorNumber 
		, pyr.TypeCode as PayorTypeCode
		, pt.Description as PayorTypeCodeDescription
		--, case when count(zeropay.number) > 0 THEN 'Y' ELSE 'N' END as ZeroPayYN
		, '' as ZeroPayYN
		, max(clm.LastReprintDate) as LastClaimDate
		, s.ServiceStatusCode
		--, '' as LastNote
		, '' as LastNonPADNoteDate
		--, MAX(pn.EnteredDate) as LastNonPADNoteDate
		--, CASE WHEN count(pyrChange.Number) > 0 THEN 'Y' ELSE 'N' END as PayorChangeMade
		, case when isnull(s.TransFromSvcNum,'') = '' THEN 'N' ELSE 'Y' END as PayorChangeMade
	from Service as s with(nolock)
		left join Provider as pro on pro.Number = s.ProviderNumber
		left join Patient as p on p.Number = s.PatientNumber
		left join ICDTen as i1 on i1.Code = s.ICDTen1
		left join ICDTen as i2 on i2.Code = s.ICDTen2
		left join Payor as pyr on pyr.Number = s.PayorNumber
		left join IHSI_PayorType as pt on pt.Code = pyr.TypeCode
		--left join PatientNote as pn on pn.PatientNumber = s.PatientNumber
		--	and pn.NoteField1 <> ''
		--	and pn.NoteField1 <> 'CALL'
		left join Claim as clm on clm.Number = s.claimnumber
		left join adjustment as adj on adj.servicenumber = s.number
			AND adj.AdjustmentTypeCode = '015'
		--left join Payment as pmt on pmt.ServiceNumber = s.Number
		--left join Adjustment as svcAdj on svcAdj.ServiceNumber = s.Number and svcAdj.TransType = 'S'
		--left join Adjustment as pmtAdj on pmtAdj.ServiceNumber = s.Number and pmtAdj.TransType = 'P'
		--left join Adjustment as pyrChange on pyrChange.ServiceNumber = s.Number and pyrChange.AdjustmentTypeCode = '015'
		--left join Payment as zeropay on zeropay.ServiceNumber = s.Number 
		--	AND zeropay.Amount = 0
		--left join adjustment as denadj on denadj.paymentnumber = zeropay.number
		--	and denadj.entereddate > zeropay.entereddate
		left join clinician as cl on cl.number = s.performedbyid
	WHERE s.Amount > 0
		and adj.number is null
		--and s.PatientNumber = '000001671'
	GROUP BY
		s.Number
		, s.ProviderNumber
		, pro.OrganizationName 
		, p.Number 
		, p.AliasName 
		, s.FromDate 
		, s.ServiceItemCode
		, s.ICDTen1 
		, i1.Description 
		, s.ICDTen2
		, i2.Description 
		, pyr.Name 
		, s.PayorNumber 
		, pyr.TypeCode 
		, pt.Description 
		, s.ServiceStatusCode
		, s.performedbyID
		, cl.name
		, s.TransFromSvcNum
		
		--, pmt.Amount
		--, pmt.EOBReasonText
	) as x
left join (
	-- ServiceAdjustments
	select s.Number, SUM(isnull(a.Amount,0)) as ServiceAdjustment--,a.ReasonText as ServiceAdjReasonText
	from Service as s
			left join adjustment as adj on adj.servicenumber = s.number
				AND adj.AdjustmentTypeCode = '015'
			left join Adjustment as a on a.ServiceNumber = s.Number and a.TransType = 'S'
		WHERE s.Amount > 0
			and adj.number is null
	group by s.Number--,a.ReasonText
	) as y on x.SvcNumber = y.Number
left join (
-- PaymentAdjustments
select s.Number, SUM(isnull(a.Amount,0)) as PaymentAdjustment--,a.ReasonText as PaymentAdjReasonText
from Service as s
		left join adjustment as adj on adj.servicenumber = s.number
			AND adj.AdjustmentTypeCode = '015'
		left join Adjustment as a on a.ServiceNumber = s.Number and a.TransType = 'P'
	WHERE s.Amount > 0
		and adj.number is null
group by s.Number--,a.ReasonText
) as z on x.SvcNumber = z.Number
LEFT JOIN (
-- Payments
select s.Number, SUM(isnull(pmt.Amount,0)) as Payments
from Service as s
		left join Payment as pmt on pmt.servicenumber = s.number
group by s.Number
) as zz on x.SvcNumber = zz.Number
LEFT JOIN (
-- zero pay
select
	Number,
	EOBReasonText,
	DateOfZeroPay
	--,
	--zeroPayReasonText 
from #tmpzeropay where Rnum=1
) as ZA on ZA.Number = x.SvcNumber
--where x.svcnumber = '000344608'
--where x.PatientNumber in ('000070022') 
order by SvcNumber 



