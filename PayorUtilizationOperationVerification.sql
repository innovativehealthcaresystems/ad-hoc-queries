select
	pay.TypeCode
	,pt.Description as PayorType
	,pay.Name
	,pay.AddressLine1
	,pay.AddressLine2
	,pay.City
	,pay.State
	,pay.ZipCode
	,pay.Number
	,pay.CPIDNumericCode
	,count(s.Number) as CodedServices
from payor pay
left join service s
	on pay.Number = s.PayorNumber
	and s.amount > 0.01
	and s.TransToSvcNum = ''
	and s.FromDate >= dateadd(year,-1,getdate())
left join IHSI_PayorType pt
	on pay.TypeCode = pt.Code
group by
	pay.TypeCode
	,pay.Name
	,pay.AddressLine1
	,pay.AddressLine2
	,pay.City
	,pay.State
	,pay.ZipCode
	,pay.Number
	,pay.CPIDNumericCode
	,pt.Description
order by CodedServices desc