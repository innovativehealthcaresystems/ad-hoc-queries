select *
into #tmpPolicyPlaceHolders
from(

	select d1.Ins1PolicyNumber
		,count(d1.Ins1PolicyNumber) as Uses
	from IHSI_DemographicImportTemplateUpdatable d1
	where DATEPART(year, d1.ServiceDate) = 2017
		and (d1.Ins1PolicyNumber in ('','0','00','000','0000','00000','SELF PAY', '*', '**')
			or (d1.Ins1PolicyNumber like '000%000'))
	group by d1.Ins1PolicyNumber
		--order by uses desc
) as TPP

select *
into #tmpCounts
from (
	/*No policy numbers received*/
	select DATEPART(month, d1.ServiceDate) as MOS
		,count(distinct d1.AliasName) as NoPolicyNumber
		,'' as PolicyNumberFound
	from IHSI_DemographicImportTemplateUpdatable d1
	join #tmpPolicyPlaceHolders TPP
		on d1.Ins1PolicyNumber = TPP.Ins1PolicyNumber
	left join IHSI_DemographicImportTemplateUpdatable d2
		on d1.AliasName = d2.AliasName
		and d1.Ins1PolicyNumber <> d2.Ins1PolicyNumber
		and d2.Ins1PolicyNumber is not null
	left join #tmpPolicyPlaceHolders TPP2
		on d2.Ins1PolicyNumber = TPP2.Ins1PolicyNumber
	where DATEPART(year, d1.ServiceDate) = 2017
		and d2.AliasName is null
		and TPP2.Ins1PolicyNumber is null
		and d1.Imported = 1
	group by DATEPART(month, d1.ServiceDate)
	--order by MOS asc
	
	union
	
	/*Policy Numbers received*/
	select DATEPART(month, d1.ServiceDate) as MOS
		,'' as NoPolicyNumber
		,count(distinct d1.AliasName) as PolicyNumberFound
	from IHSI_DemographicImportTemplateUpdatable d1
	left join #tmpPolicyPlaceHolders TPP
		on d1.Ins1PolicyNumber = TPP.Ins1PolicyNumber
	--left join IHSI_DemographicImportTemplateUpdatable d2
	--	on d1.AliasName = d2.AliasName
	--	and d1.Ins1PolicyNumber <> d2.Ins1PolicyNumber
	--left join #tmpPolicyPlaceHolders TPP2
	--	on d2.Ins1PolicyNumber = TPP2.Ins1PolicyNumber
	where DATEPART(year, d1.ServiceDate) = 2017
		and tpp.Ins1PolicyNumber is null
		and d1.Imported = 1
		--and d2.AliasName is null
		--and TPP2.Ins1PolicyNumber is null
	group by DATEPART(month, d1.ServiceDate)
	--order by MOS asc
) as TC

select TC.MOS as MonthOfService
	,sum(TC.NoPolicyNumber) as MissingPolicyNumbers
	,sum(TC.PolicyNumberFound) as PolicyNumberFound
	,sum(TC.NoPolicyNumber) + sum(TC.PolicyNumberFound) as TotalPatients
from #tmpCounts TC
group by TC.MOS
order by TC.MOS asc

drop table #tmpCounts
drop table #tmpPolicyPlaceHolders