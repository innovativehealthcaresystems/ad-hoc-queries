select distinct c.PayorNumber
	,pyr.Name as PayorName
	,c.Number as ClaimNumber
	,cast(c.PrintedDate as date) as ClaimPrintedDate
	,cast(c.AdmittedDate as date) as DateOfService
	,c.BilledAmount
	,c.PatientNumber
	,p.Name as PatientName
	,pp.PolicyNumber
	,pp.GroupNumber
	,bbp.Balance as AccountBalance
	,isnull(sum(pay.PayAmount + pay.AdjAmount), 0.00) as PaymentsFromPayors8And11
from claim c
join PatientPayor pp
	on c.PayorNumber = pp.PayorNumber
	and c.PatientNumber = pp.PatientNumber
	and LEFT(pp.PolicyNumber, 3) = 'IHQ'
	and c.PrintedDate >= '2018-01-01'
join Payor pyr
	on c.PayorNumber = pyr.Number
left join IHSI_BalanceByPatient bbp
	on c.PatientNumber = bbp.PatientNumber
left join service s
	on c.PatientNumber = s.PatientNumber
	and s.Amount * s.Units > 0.01
left join IHSI_MGTPayments pay
	on s.Number = pay.svcnumber
	and pay.pmtpayor in ('0000008', '0000011')
left join Patient p
	on c.PatientNumber = p.Number
group by c.PayorNumber
	,pyr.Name
	,c.Number
	,cast(c.PrintedDate as date) 
	,cast(c.AdmittedDate as date)
	,c.BilledAmount
	,c.PatientNumber
	,pp.PolicyNumber
	,pp.GroupNumber
	,bbp.Balance
	,p.Name
order by c.PayorNumber asc
	,c.PatientNumber asc
	,c.Number asc
--order by bbp.Balance asc
