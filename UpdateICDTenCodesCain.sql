/*
Uncomment and run the below scripts on Cain to import a file into the IHS_DB and parse it out.
*/

--create table ICDUpload (LongDesc char(400) NOT NULL)
--GO




--BULK INSERT ICDUpload
--    --FROM '\\Melia\c$\Temp\ICD.txt' 
--    FROM '\\cain\c$\Temp\ICDUpdate.txt' 
--    WITH 
--    ( 
--        FIELDTERMINATOR = '\t', 
--        ROWTERMINATOR = '\n' 
--    )


--select *
--from ICDUpload


--create table ICDUpload2
--(
--	OrderNumber char(5) NOT NULL,
--	Code char(8) NOT NULL,
--	header char(1) NOT NULL,
--	ShortDesc char(65) NOT NULL,
--	LongDesc char(255) NOT NULL
--)
--GO


--insert into ICDUpload2
--	(
--	OrderNumber,
--	Code,
--	header,
--	ShortDesc,
--	LongDesc 
--	)
--select left(longDesc,5) as OrderNumber
--	,rtrim(substring(longdesc,7,7)) as Code
--	,rtrim(substring(longdesc,15,1)) as Header
--	,rtrim(substring(longdesc,17,60)) as ShortDesc
--	,rtrim(substring(longdesc,78,400)) as LongDesc
--from ICDUpload


--update ICDUpload2
--set Code = Stuff(Code, 4, 0, '.')
--where len(code)> 3
--	and Header = '1'

/*
Once you are done updating the client db's clean up your mess.
*/
----drop table icdupload
----drop table icdupload2
----drop table icdtendeactivation
