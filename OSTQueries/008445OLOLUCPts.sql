select *
into #tmpPatients
from (
	select p.Number
		,p.AliasName
		,p.MedicalRecordNo
		,p.Name
		,cast(s.FromDate as date) as DateOfService
		,sum(s.Amount * s.Units) as Charges
		,count(s.ServiceItemCode) as SICodeCount
	from service s
	left join adjustment a
		on s.Number = a.ServiceNumber
		and a.AdjustmentTypeCode = '019'
	join patient p
		on s.PatientNumber = p.Number
	where s.FromDate >= '2018-01-01'
		and s.FromDate < '2019-02-01'
		and s.TransToSvcNum = '' --Filter transferred services
		and a.ServiceNumber is null --Filter invalid services
		and s.ProviderNumber = '0000008' --LSU HBRNC
	group by p.Number
		,p.AliasName
		,p.MedicalRecordNo
		,p.Name
		,cast(s.FromDate as date)
) TP

select tp.Number as PatientNumber
	,tp.AliasName as AccountNumber
	,tp.MedicalRecordNo
	,tp.Name
	,tp.DateOfService
	,tp.Charges
	,isnull(s.ServiceItemCode, 'N/A') as Pseudocode
from #tmpPatients tp
left join service s
	on tp.Number = s.PatientNumber
	and tp.SICodeCount = 1 --Only want patients with a single pseudocode
	and tp.Charges = 0
	and tp.DateOfService = cast(s.fromdate as date)
	and s.FromDate >= '2018-01-01'
	and s.FromDate < '2019-02-01'
	and s.TransToSvcNum = '' --Filter transferred services
	and s.ProviderNumber = '0000008' --LSU HBRNC
left join adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '019'
where a.ServiceNumber is null --Filter invalid services
order by tp.DateOfService asc
	,tp.AliasName asc

drop table #tmpPatients
