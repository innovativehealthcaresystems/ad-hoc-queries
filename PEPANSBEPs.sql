--USE [LA_OLOL_PEPA]
--GO
--/****** Object:  StoredProcedure [dbo].[IHSI_CodingQueueByHoursQueue]    Script Date: 8/28/2018 4:30:16 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--ALTER PROCEDURE [dbo].[IHSI_CodingQueueByHoursQueue]
--	@ProviderGroupNumber int,
--	@CodingBufferHourDelay tinyint = 0,
--	@CodingBufferDelayAllPayors bit = 0
--AS

/*
-- =============================================
-- Author:		Sriram.C
-- Create date: 09-MAY-2017
-- Description:	Created for the modified ICIS ED Coding Screen in version 1.1.1.100 per SR 40179. It takes the user to the most recent chart based on BCD date
--				when the user clicks 'Code Available Charts'. It also populates the coding screen when the user checks 'Show Queue'.
-- Note:		
-- Entity Type: ED Coding Queue
--
-- Last Modified:

	- 7/17/2017 by Margaret W. Added a get out of purgatory filter for records without all images that are a certain # of days old for PEMM DBs.
	
	- 7/15/2017 by Justin T. and Margaret - Added logic to keep things out of the queue that have facesheets but no charts for PEMM DBs.

	- 6/22/2017 by Sriram C. Added IHSI_PayorTypeQueueOrder left join 

	- 6/8/2017 by Sriram C.
	
	- 5/31/2017 by Justin T. 
 
-- =============================================
IHSI_CodingQueueByHoursQueue 1
*/

------------------ TESTING -------------------
DECLARE 
	@ProviderGroupNumber int,
	@CodingBufferHourDelay tinyint = 0,
	@CodingBufferDelayAllPayors bit = 0
SET @ProviderGroupNumber = 0 -- All Providers
--SET @ProviderGroupNumber = 1 -- Individual Provider
---------------------------------------------

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @ProviderGroupNumberTable TABLE (ProviderNumber CHAR (7))--Added by Margaret on 7/16/2017 to avoid having 4 CTE IF statement blocks after having to alter this SPROC for the PEMM DB
IF @ProviderGroupNumber <> 0
BEGIN
	INSERT INTO @ProviderGroupNumberTable 
	SELECT pga.ProviderNumber FROM IHSI_ProviderGroup as PG --on pg.Number = @ProviderGroupNumber		--Removed by Justin on 7/16/2017
	INNER JOIN IHSI_ProviderGroupAssignment PGA on pg.Number = pga.GroupNumber
	WHERE GroupNumber = @ProviderGroupNumber
END
ELSE 
BEGIN 
	INSERT INTO @ProviderGroupNumberTable
	SELECT DISTINCT ProviderNumber FROM dbo.IHSI_Staging
END


DECLARE @today DATETIME =  DATEADD(DAY, DATEDIFF(DAY,0, GETDATE()), 0)
DECLARE @PurgatoryDays INT = CASE WHEN DB_NAME() IN 
('')
--('LA_WCCH_CCEPG','LA_WCCH_CCHMG')	--Removed by Justin per Jeff 
THEN -11 ELSE -6 END 
--12 days for CCEPG/CCHMG, 7 days for all other PEMM DBs. The logic takes the current day @midnight. 
--The filtering logic down below takes records that are less than the current day at midnight -6 (or whatever the variable is) days to get all records
--that are 7 days old, regardless of time (records that are older than 6 days ago at midnight)
--added by Margaret on 7/17/2017 to account for variable days in purgatory 
--this will need to be revised later for other DBs that need purgatory logic 


--IF DB_NAME ()IN 
--('')
----('LA_LCMH_PEMMLC','LA_OLOA_WPEPG','LA_STPH_STEPG','LA_TRMC_TEPG','LA_WCCH_CCEPG','LA_WCCH_CCHMG')--added by Margaret on 7/16/2017 to account for PEMM DBs	--Removed by Justin per Jeff
--BEGIN 	
--	select
--		st.Staging_ID,
--		Case
--			WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
--			WHEN MAX(ISNULL (I.STAGEDDATE, '1900-01-01'))<=ST.STAGEDDATE THEN MAX(ST.STAGEDDATE)	
--			--Modified by Justin T. on 5/31/2017 - changed output from MAX(I.STAGEDDATE) to MAX(ST.STAGEDDATE)
--			--Modified by Margaret on 7/19/2017 - added is null to account for records moved back from purgatory 
--		END AS MostRecentDate,
--		st.ti_dateofservice,
--		st.TI_AccountNumber,
--		ISNULL(p1.typecode, 'XX') AS PayorType, st.PatientNumber,
--		'NA' as SkipReason, 'NA' as StatusCheck, 0 AS CodeCount , pp1.PayorNumber
--	from IHSI_Staging as st
--		left join IHSI_ImageFiles as i on st.Staging_ID=i.Staging_ID
--			AND CHARINDEX('_DEM_', i.ImageFileName) = 0		--Added 7/15/2017 by Justin T. to make sure there is an actual chart associated with the record rather than just a facesheet
--		LEFT JOIN PatientPayor as PP1 on pp1.patientnumber = st.patientnumber AND pp1.type = 'P'
--		LEFT JOIN Payor as P1 on P1.number = pp1.payornumber 
--		LEFT JOIN IHSI_PayorTypeQueueOrder as PT on ISNULL(p1.typecode, 'XX') = PT.PayorType AND PT.Screen = 'Coding'
--		LEFT JOIN IHSI_SkipLog as sl with(nolock) on st.Staging_ID=sl.Staging_ID
--			and sl.ReviewComplete = 'Y' AND sl.Active = 'Y'
--		LEFT JOIN IHSI_SkipReasons sr with(nolock) on sl.SkipReasonCode = sr.SkipCode
--	where 
--		(st.Skipped in ('N') or (st.Skipped in ('Y') and sr.skiptype IN ('C', 'D', 'X', 'Y')))
--		and st.Coded='N'
--		and st.CodingLock = 'N'			--- Added 5/31/17 by Justin T. as found in procedure IHSI_CodingQueue
--		and ST.ProviderNumber IN (SELECT ProviderNumber FROM @ProviderGroupNumberTable)
--		and ((DATEADD(Hour, @CodingBufferHourDelay, st.StagedDate) < GETDATE()) OR (@CodingBufferDelayAllPayors <> 1 AND pp1.PayorNumber <> '0000001') OR st.CodingTier < 3)
--	Group by st.StagedDate,st.Staging_ID,st.TI_AccountNumber, st.PatientNumber,p1.typecode, pp1.PayorNumber,st.ti_dateofservice
--	having (COUNT(i.image_id)>=1) OR (st.StagedDate < DATEADD(DD, @PurgatoryDays, @today)) --added by Margaret to get records out of purgatory 
--	order by MostRecentDate asc, CASE WHEN @ProviderGroupNumber <> 0 THEN NEWID() END
--END
--ELSE
BEGIN
	select
		st.Staging_ID,
		Case
			WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
			WHEN MAX(i.StagedDate)<=st.stagedDate THEN MAX(st.StagedDate) --Modified by Justin T. on 5/31/2017 - changed output from MAX(I.STAGEDDATE) to MAX(ST.STAGEDDATE)
		END AS MostRecentDate,
		st.ti_dateofservice,
		st.TI_AccountNumber,
		ISNULL(p1.typecode, 'XX') AS PayorType, st.PatientNumber,
		'NA' as SkipReason, 'NA' as StatusCheck, 0 AS CodeCount , pp1.PayorNumber
	from IHSI_Staging as st
		LEFT JOIN IHSI_ImageFiles as i on st.Staging_ID=i.Staging_ID
		LEFT JOIN PatientPayor as PP1 on pp1.patientnumber = st.patientnumber AND pp1.type = 'P'
		LEFT JOIN Payor as P1 on P1.number = pp1.payornumber 
		LEFT JOIN IHSI_PayorTypeQueueOrder as PT on ISNULL(p1.typecode, 'XX') = PT.PayorType AND PT.Screen = 'Coding'
		LEFT JOIN IHSI_SkipLog as sl with(nolock) on st.Staging_ID=sl.Staging_ID
			and sl.ReviewComplete = 'Y' AND sl.Active = 'Y'
		LEFT JOIN IHSI_SkipReasons sr with(nolock) on sl.SkipReasonCode = sr.SkipCode
	where (st.Skipped in ('N') or (st.Skipped in ('Y') and sr.skiptype IN ('C', 'D', 'X', 'Y')))
		and st.Coded='N'
		and st.CodingLock = 'N'			--- Added 5/31/17 by Justin T. as found in procedure IHSI_CodingQueue
		and ST.ProviderNumber IN (SELECT ProviderNumber FROM @ProviderGroupNumberTable)
		and ((DATEADD(Hour, @CodingBufferHourDelay, st.StagedDate) < GETDATE()) OR (@CodingBufferDelayAllPayors <> 1 AND pp1.PayorNumber <> '0000001') OR st.CodingTier < 3)

		and (i.ImageFileName like '%nsbep%'
or i.imagefilename like '%7XXXX%'
or i.imagefilename like '%8XXXX%'
or i.imagefilename like '%ADMIT%'
or i.imagefilename like '%COLCT%'
or i.imagefilename like '%DAINS%'
or i.imagefilename like '%DISCT%'
or i.imagefilename like '%DOCUM%'
or i.imagefilename like '%G0168%'
or i.imagefilename like '%INTST%'
or i.imagefilename like '%LTAMA%'
or i.imagefilename like '%LWOBS%'
or i.imagefilename like '%MVACC%'
or i.imagefilename like '%NSBEP%'
or i.imagefilename like '%PAYMT%'
or i.imagefilename like '%PSIGN%'
or i.imagefilename like '%SANEX%'
or i.imagefilename like '%STORM%'
or i.imagefilename like '%SUTUR%'
or i.imagefilename like '%TRANS%'
or i.imagefilename like '%UNBIL%'
or i.imagefilename like '%VOID1%'
or i.imagefilename like '%WCOMP%'
)

	Group by st.StagedDate,st.Staging_ID,st.TI_AccountNumber, st.PatientNumber,p1.typecode, pp1.PayorNumber,st.ti_dateofservice
	having COUNT(i.image_id)>=1
	order by MostRecentDate asc, CASE WHEN @ProviderGroupNumber <> 0 THEN NEWID() END
END	
END

